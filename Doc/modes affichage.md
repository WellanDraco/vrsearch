# Modes d'affichage

L'objectif de la plateforme est d'etre assez flexible pour etre utilisée comme moteur de recherche sur une grande variété de plateforme

- Ordinateur 2d
- Ordinateur 2d avec multi-écran
- Mobile 2d
- casque immersif avec vue navigateur 2D
- casque immersif en session immersive webXR

|                  | 2D                 | Immergé                |
| ---------------: | ------------------ | ---------------------- |
| **Ecran unique** | cas par défaut (1) | Mode concentration (3) |
|  **Multi-écran** | mode recherche (2) | Mode productivité (4)  |

Ordre de priorité : 1, 2, 4, 3

Fin de session (0)

---

## 1: Cas par défaut
Version que tout les utilisateurs vont voir en premier.
Elle doit donner envie à l'utilisateur de l'utiliser, voir de l'essayer dans un casque immersif. 

- Si l'utilisateur utilise un ordinateur ou un casque immersif, Il peut choisir le mode 2 durant la recherche. 
  > Cela ouvrira d'autres pages web avec les résultats attendus que l'utilisateur pourra placer à sa guise dans son espace de travail.

- Si l'utilisateur se trouve dans un casque immersif, on peut promouvoir les modes 3 et 4. 
  > Cela lancera une session immersive et transformera les résultats actuels en rendu 3D.

## 2: Mode recherche 
L'utilisateur utilise un ordinateur ou est dans un casque immersif avec plusieurs fenêtres ouvertes. 

- Si l'utilisateur veux finir sa session et tout fermer, on doit intégrer l'option de mode 0.
  > Cela fermera toutes les fenêtres du site.

- Si l'utilisateur est en casque immersif, on peut promouvoir le mode 4.
  > Cela fermera toutes les autres fenêtres et les rouvrira en mode 3D.

## 3: Mode concentration
L'utilisateur a lancé une session immersive et est fasse à une page de recherche.

- L'utilisateur peut quitter l'immersion et revenir en mode 1. 
  > Cela cachera le rendu 3d et laissera apparaitre la version web.

- Durant la recherche, il peut choisir le mode 4. -
  > Cela génèrera d'autres fenêtres 3d au fils de la recherche.

## 4: Mode productivité
L'utilisateur doit pouvoir profiter un maximum de son environnement pour etre efficace et productif au seins de l'application.

- L'utilisateur peut quitter l'immersion en mode 2. 
  > Les pages 3D sont réouvertes en tant que pages séparées dans le navigateur.
  
- L'utilisateur peut quitter l'immersion en mode 1. 
  > Cela cachera le rendu 3d et laissera apparaitre la version condensée web.

- L'utilisateur peut regrouper tous ses résultats pour le mode 3.
  > cela regroupera toutes les fenêtres 3d en une.