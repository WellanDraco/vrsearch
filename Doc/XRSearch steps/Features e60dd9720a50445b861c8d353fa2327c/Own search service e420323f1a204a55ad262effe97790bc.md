# Own search service

Depend On: Basic%20search%20service%20816aea8b913e47a694003d58f6c48e9e.md, Indexing%20Flat%20and%20Immersive%20content%20520320c44054450b92baa655c6881966.md, Scrapping%20native%20content%20b4876c06c4f447a2ab8abbc75a5b46ba.md
Phase: Phase 2 Owning the Search
Required for: Affiliation%20of%20asset%20marketplaces%20305107d4f4994ee595c88ae39e9bcdc2.md, Search%20integration%20in%20proprietary%20platforms%206eeb8cc77e3543dc93885b63bb5ad0c7.md, Promoted%20results%20582cecb973544bd688173ab6c814e64e.md
Skills Required: APIs
category: BackEnd, SearchAPI