# Multiple queries and result displays

Depend On: Basic%20Query%20and%20result%20display%2008e183bcb1de4f3a968d356d9605eaf4.md, Flat%20UX%20890c3221dbc94178a90525a39b018fe1.md
Phase: Phase 1 Providing the Search
Required for: Multiple%20Windows%2016e5141cb60e4300bc7cc177abbdfe77.md
Skills Required: 3D stack, Web stack
category: FrontEnd, Productivity

[https://cdn-icons-png.flaticon.com/512/1066/1066419.png](https://cdn-icons-png.flaticon.com/512/1066/1066419.png)