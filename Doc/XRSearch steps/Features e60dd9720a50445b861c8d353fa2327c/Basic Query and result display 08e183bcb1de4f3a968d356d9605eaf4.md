# Basic Query and result display

Depend On: Basic%20search%20service%20816aea8b913e47a694003d58f6c48e9e.md
Phase: Phase 1 Providing the Search
Required for: Multiple%20queries%20and%20result%20displays%20965d6d8391de444a84fe1a4ed531838c.md, Advertizing%20with%20Zesty%20Market%2096364e8e28114d8d9cbb1a0f6caaa73c.md, Multimedia%20player%201cc0c80882e346bfb95d6f15e8fcb268.md, Immersive%20content%20signaling%20f828c61583a24f6788d98d1575f9975a.md, Vocal%20search%206b7b248126f244518fa8671aedbae821.md
Skills Required: APIs, Web stack
category: FrontEnd