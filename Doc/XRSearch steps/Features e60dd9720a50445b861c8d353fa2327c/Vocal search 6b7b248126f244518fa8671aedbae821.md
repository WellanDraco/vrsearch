# Vocal search

Depend On: Basic%20Query%20and%20result%20display%2008e183bcb1de4f3a968d356d9605eaf4.md
Phase: Phase 1 Providing the Search
Skills Required: APIs, Web stack
category: FrontEnd, Optional

![20211018_172648.jpg](Vocal%20search%206b7b248126f244518fa8671aedbae821/20211018_172648.jpg)

[https://azure.microsoft.com/en-us/services/cognitive-services/speech-to-text/](https://azure.microsoft.com/en-us/services/cognitive-services/speech-to-text/)