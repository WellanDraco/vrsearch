# Immersive content signaling

Depend On: Basic%20Query%20and%20result%20display%2008e183bcb1de4f3a968d356d9605eaf4.md, Indexing%20Flat%20and%20Immersive%20content%20520320c44054450b92baa655c6881966.md
Phase: Phase 3 Super Searching
Required for: Immersive%20content%20rating%20911807214ce44b96847623b5d82c3c11.md, Immersive%20navigation%209b8ed33664914a46ab7f6436236f9fa8.md
Skills Required: APIs, SEO
category: FrontEnd, Metaverse