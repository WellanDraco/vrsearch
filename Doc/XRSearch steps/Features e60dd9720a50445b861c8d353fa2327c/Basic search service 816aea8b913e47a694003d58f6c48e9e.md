# Basic search service

Depend On: Bing%20Integration%20edf6c7ead1c64d229da39df9201daf27.md
Phase: Phase 1 Providing the Search
Required for: Basic%20Query%20and%20result%20display%2008e183bcb1de4f3a968d356d9605eaf4.md, DuckDuckGo%20integration%207ff818810ac14910bb0c5c1275892cf0.md, Own%20search%20service%20e420323f1a204a55ad262effe97790bc.md
Skills Required: APIs
category: BackEnd