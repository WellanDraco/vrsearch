# Search integration in proprietary platforms

Depend On: Own%20search%20service%20e420323f1a204a55ad262effe97790bc.md, XR%20UX%2013459780827b4d26bbccc617ae345167.md
Phase: Phase 4 Inside the Metaverse
Skills Required: 3D stack, APIs
category: BtB, External, Metaverse, SearchAPI