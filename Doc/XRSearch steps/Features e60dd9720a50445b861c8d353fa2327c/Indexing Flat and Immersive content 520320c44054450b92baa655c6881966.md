# Indexing Flat and Immersive content

Phase: Phase 2 Owning the Search
Required for: Own%20search%20service%20e420323f1a204a55ad262effe97790bc.md, Immersive%20content%20signaling%20f828c61583a24f6788d98d1575f9975a.md
Skills Required: APIs, SEO
category: BackEnd, Indexing, Metaverse