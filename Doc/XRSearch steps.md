# [XRSearch steps](https://www.notion.so/XRSearch-steps-414ee63d0d9b4b7d9f1a457ab5a72d2c)

![image-1633941789824.jpg131699188793420993.jpg](XRSearch%20steps/image-1633941789824.jpg131699188793420993.jpg)

- XRSearch feature tree
    - Frontend
        - Productivity
            - Multiple windows
            - Multiple Queries
            - Note-taking
        - Cross-Platform
            - Flat UX
            - Immersive UX
        - Privacy
            - Choosable search engine
                - Default Bing
                - Add own search service Later
                - Add DuckDuckGo Later ?
            - Advertising
                - Zesty.market
                - Promoted results based on query/history/personal preferences
                - 3D models in space ad advertising
        - Multimedia viewer
            - Web ? (RSS, iframe?)
            - Video (flat, stereo, 180, 360...)
            - Audio (mono, stereo...)
            - 3d models (read animations, avatars)
            - AR in VR ?
        - Immersive Content promotion
            - Identification of said content
            - Rating of immersive experiences
            - Immersive navigation
        - Multiplayer
            - Messaging
            - Immersive social
            - Join your friends on their social space
        - Customization Marketplace
            - Buy avatar
            - Buy environment
                - Pay premium for not having 3D ads on it
    - Our search service
        - Scrapping web
        - Scrapping native apps
        - Indexing flat and immersive content
        - Affiliation for asset marketplaces
    - Metaverse
        - Indexation SDK for native content and platforms
        - In-platform Search Result integration
            - NFT integration
            - SDK for proprietary setup
            - Meta-chromium ?

### Phase Projet

1. Web searching
    1. MultiTasking and productivity
    2. Customization and monetization
2. Owning the search
    1. Indexing the Metaverse
    2. Monetizing the search
    3. Offering privacy solutions
3. Super Searching
    1. Immersive web promotion
    2. SocialVerse
    3. Super Productivity
4. Searching inside the Metaverse

---

[Features](XRSearch%20steps%20ef335b07aa51462094794562757232c8/Features%20e60dd9720a50445b861c8d353fa2327c.csv)