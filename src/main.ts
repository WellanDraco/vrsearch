import {createApp} from 'vue'
import App from './App.vue'
import router from './router/Router'
import { createPinia } from 'pinia'

const createdApp = createApp(App).use(createPinia()).use(router).use(createPinia())
createdApp.mount('#app')

window.onbeforeunload = function () {
	createdApp.unmount();
}
