import {createRouter, createWebHistory} from 'vue-router'
//import Results from '../views/Results.vue'
import SearchView from '../views/SearchView.vue'

const routes = [
	{
		path: '/',
		name: 'results',
		components: {
			default: SearchView,
		}
	},
	{
		path: '/about',
		name: 'About',
		// route level code-splitting
		// this generates a separate chunk (about.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
	},
	{
		path: "/:pathMatch(.*)*",
		name: '404',
		component: () => import(/* webpackChunkName: "404" */ '../views/FailedRoute.vue')
	}
]

const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes
})

export default router
