import { defineStore } from 'pinia'
import {Pinia} from "pinia";
import _ from "lodash";

export interface Type {
	name:string,
	value:string,
	used:boolean
}
interface State {
	autocompletion: boolean,
	openInNewPage:boolean,
	immersed:boolean,
	types:Array<Type>,
	results:any[]
}

export const store = defineStore('global/state',{
	state: (): State => ({
		autocompletion: true,
		openInNewPage: false,
		immersed: false,
		types: [
			{name: "Webpages", value: "Web", used: true},
			{name: "Images", value: "Image", used: true},
			{name: "Videos", value: "Video", used: true},
			{name: "Metaverse", value: "Metaverse", used: false},
		],
		results: []
	}),
	getters: {
	},
	actions: {
		getTypeName (value:string) {
			return this.types.filter(type => type.value === value)[0].name;
		},
		getTypesNames (values:string[]) {
			//return multiple elements
			const response:string[] = [];
			values.forEach(value => {
				response.push(this.getTypeName(value));
			})
			return response;
		},
		getTypeNamesFromTypeString(string:string) {
			const values = string.split("&");
			return this.getTypesNames(values);
		},
		getTypeValue(name:string) {
			return this.types.filter(type => type.name === _.capitalize(name))[0].value;
		},
		switchAutocomplete(val:boolean) {
			this.autocompletion = val
		},
		switchOpenInNewPage(val:boolean) {
			this.openInNewPage = val
		},
		switchImmersed(val:boolean) {
			this.immersed = val
		},
		addResults(val:any) {
			//if the query result is new, then we create a new object
			if (!this.results[val.query])
				this.results[val.query] = [];
			//then we add to the object the new type with it's content
			this.results[val.query][val.type] = val.content;
		},
		async callApi(parameters:any) {
			//php api calls
			//console.log(commit, parameters);
			const typenames = this.getTypeNamesFromTypeString(parameters.type);
			const queryData = await fetch(`http://localhost:8000/apiv2.php?q=${parameters.query}&responseFilter=${typenames.join(',')}`);
			const data = await queryData.json();
			//console.log(data);
			for (const dataElementKey in data[1]) {
				//filter those that are not a type of data
				if (['_type', 'queryContext', 'rankingResponse'].some(e => e === dataElementKey)) continue;
				//For any datatype, we add them to the global state
				this.addResults({query: parameters.query, type: dataElementKey, content: data[1][dataElementKey]})
			}

		}

	}
})

export default store
