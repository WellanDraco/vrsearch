<?php

//error_reporting(E_ERROR);
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

header('Content-Type: application/json');

if(isset($_GET['clientId'])) $clientId=$_GET['clientId']; 
if(isset($_GET['ClientIP'])) $clientId=$_GET['ClientIP']; 
$q= (isset($_GET['q'])) ? $_GET['q'] : "bonjour";

//Off - Moderate - Strict
$safeSearch = (isset($_GET['safeSearch'])) ? $_GET['safeSearch'] : "Moderate";

//Country code
$cc  = (isset($_GET['cc'])) ? $_GET['cc'] : "us";

$responseFilter = (isset($_GET['responseFilter'])) ? $_GET['responseFilter'] : "Computation,Entities,Images,News,Places,RelatedSearches,SpellSuggestions,Videos,Webpages";
$parameters = [$q,$safeSearch,$cc,$responseFilter];


$accessKey = '2f0d6d44db9c4a499d061ec6a910c15e';
$endpoint = 'https://vr-search-engine.cognitiveservices.azure.com/bing/v7.0/search';
//$endpoint = 'https://api.cognitive.microsoft.com/bing/v7.0/search';
$term = 'Microsoft Cognitive Services';
$fakeResults = '{
    "_type": "SearchResponse",
    "queryContext": {
      "originalQuery": "Microsoft Cognitive Services"
    },
    "webPages": {
      "webSearchUrl": "https://www.bing.com/search?q=Microsoft+cognitive+services",
      "totalEstimatedMatches": 22300000,
      "value": [
        {
          "id": "https://api.cognitive.microsoft.com/api/v7/#WebPages.0",
          "name": "Microsoft Cognitive Services",
          "url": "https://www.microsoft.com/cognitive-services",
          "displayUrl": "https://www.microsoft.com/cognitive-services",
          "snippet": "Knock down barriers between you and your ideas. Enable natural and contextual interaction with tools that augment users\' experiences via the power of machine-based AI. Plug them in and bring your ideas to life.",
          "deepLinks": [
            {
              "name": "Face",
              "url": "https://azure.microsoft.com/services/cognitive-services/face/",
              "snippet": "Add facial recognition to your applications to detect, identify, and verify faces using the Face service from Microsoft Azure. ... Cognitive Services; Face service;"
            },
            {
              "name": "Text Analytics",
              "url": "https://azure.microsoft.com/services/cognitive-services/text-analytics/",
              "snippet": "Cognitive Services; Text Analytics API; Text Analytics API . Detect sentiment, ... you agree that Microsoft may store it and use it to improve Microsoft services, ..."
            },
            {
              "name": "Computer Vision API",
              "url": "https://azure.microsoft.com/services/cognitive-services/computer-vision/",
              "snippet": "Extract the data you need from images using optical character recognition and image analytics with Computer Vision APIs from Microsoft Azure."
            },
            {
              "name": "Emotion",
              "url": "https://www.microsoft.com/cognitive-services/en-us/emotion-api",
              "snippet": "Cognitive Services Emotion API - microsoft.com"
            },
            {
              "name": "Bing Speech API",
              "url": "https://azure.microsoft.com/services/cognitive-services/speech/",
              "snippet": "Add speech recognition to your applications, including text to speech, with a speech API from Microsoft Azure. ... Cognitive Services; Bing Speech API;"
            },
            {
              "name": "Get Started for Free",
              "url": "https://azure.microsoft.com/services/cognitive-services/",
              "snippet": "Add vision, speech, language, and knowledge capabilities to your applications using intelligence APIs and SDKs from Cognitive Services."
            }
          ]
        }
      ]
    },
    "relatedSearches": {
      "id": "https://api.cognitive.microsoft.com/api/v7/#RelatedSearches",
      "value": [
        {
          "text": "microsoft bot framework",
          "displayText": "microsoft bot framework",
          "webSearchUrl": "https://www.bing.com/search?q=microsoft+bot+framework"
        },
        {
          "text": "microsoft cognitive services youtube",
          "displayText": "microsoft cognitive services youtube",
          "webSearchUrl": "https://www.bing.com/search?q=microsoft+cognitive+services+youtube"
        },
        {
          "text": "microsoft cognitive services search api",
          "displayText": "microsoft cognitive services search api",
          "webSearchUrl": "https://www.bing.com/search?q=microsoft+cognitive+services+search+api"
        },
        {
          "text": "microsoft cognitive services news",
          "displayText": "microsoft cognitive services news",
          "webSearchUrl": "https://www.bing.com/search?q=microsoft+cognitive+services+news"
        },
        {
          "text": "ms cognitive service",
          "displayText": "ms cognitive service",
          "webSearchUrl": "https://www.bing.com/search?q=ms+cognitive+service"
        },
        {
          "text": "microsoft cognitive services text analytics",
          "displayText": "microsoft cognitive services text analytics",
          "webSearchUrl": "https://www.bing.com/search?q=microsoft+cognitive+services+text+analytics"
        },
        {
          "text": "microsoft cognitive services toolkit",
          "displayText": "microsoft cognitive services toolkit",
          "webSearchUrl": "https://www.bing.com/search?q=microsoft+cognitive+services+toolkit"
        },
        {
          "text": "microsoft cognitive services api",
          "displayText": "microsoft cognitive services api",
          "webSearchUrl": "https://www.bing.com/search?q=microsoft+cognitive+services+api"
        }
      ]
    },
    "rankingResponse": {
      "mainline": {
        "items": [
          {
            "answerType": "WebPages",
            "resultIndex": 0,
            "value": {
              "id": "https://api.cognitive.microsoft.com/api/v7/#WebPages.0"
            }
          }
        ]
      },
      "sidebar": {
        "items": [
          {
            "answerType": "RelatedSearches",
            "value": {
              "id": "https://api.cognitive.microsoft.com/api/v7/#RelatedSearches"
            }
          }
        ]
      }
    }
}';


function BingWebSearch($url, $key, $query,$fakeResults, $parameters) {
    /* Prepare the HTTP request.
     * NOTE: Use the key 'http' even if you are making an HTTPS request.
     * See: http://php.net/manual/en/function.stream-context-create.php.
     */
    $headers = "Ocp-Apim-Subscription-Key: $key\r\n";
    if(isset($clientId)) {
      $headers += "X-MSEdge-ClientID: $clientId\r\n";
    }
    if(isset($ClientIP)) {
      $headers += "X-MSEdge-ClientIP: $ClientIP\r\n";
    }
    $options = array ('http' => array (
        'header' => $headers,
        'method' => 'GET'));
    
    $parametersString = "?q=" . urlencode($query);

    foreach ($parameters as $k => $v) {
        $parametersString .= "&" . $k . "=" . urlencode($v);
    }
    
    // Perform the request and get a JSON response.
    $context = stream_context_create($options);
    try {
        $result = json_decode(file_get_contents($url . $parametersString, false, $context));
    } catch (Throwable $th) {
        $result = $th;
    }

    // Extract Bing HTTP headers.
    $headers = array();
    if($http_response_header){
      foreach ($http_response_header as $k => $v) {
        $h = explode(":", $v, 2);
        if (isset($h[1]))
            if (preg_match("/^BingAPIs-/", $h[0]) || preg_match("/^X-MSEdge-/", $h[0]))
                $headers[trim($h[0])] = trim($h[1]);
      }
    }
   
    

    return json_encode(array($headers, $result));
}



echo BingWebSearch($endpoint,$accessKey,$q,$fakeResults, $parameters);