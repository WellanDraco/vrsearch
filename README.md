# xr-search
Moteur de recherche immersif.

Le projet consiste à offrir aux utilisateurs un outil de recherche et de productivité au seins d'un espace immersif.

## Objectif

- Fonctionne sur ordinateur, smartphone et matériel immersif
- Améliore la productivité
- S'adapte à l'utilisateur (anonymat, internationnalisation, customisation)
- Intégration avec le métaverse WebXR
  


  

---
### Project setup
```
npm install 
```

#### Compiles and hot-reloads Vuejs for development
```
npm run serve
```

#### Run php server
```
php -S localhost:8000
```

#### Compiles and minifies for production
```
npm run build
```

#### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

[XRSearch steps](https://www.notion.so/XRSearch-steps-414ee63d0d9b4b7d9f1a457ab5a72d2c)


