//code from https://github.com/poch3ng/aframe-flexlayout/blob/master/aframe-flexwidget.js
//Improved to implement troika text
AFRAME.registerComponent("clipping-volume", {
    schema: {
        exclude: { type: "selector", default: null },
        clipTop: { default: true },
        clipBottom: { default: true },
        clipLeft: { default: false },
        clipRight: { default: false },
    },
    init() {
        this.el.sceneEl.renderer.localClippingEnabled = true;
        this._clippingPlanesLocal = [];
        this._clippingPlanes = [];
        this._currentMatrix = null;
        this._filterEvent = this._filterEvent.bind(this);
        this._filterTargets = [
            "click",
            "mousedown",
            "mouseenter",
            "mouseleave",
            "mousemove",
        ];
        this._filterTargets.forEach((t) =>
            this.el.addEventListener(t, this._filterEvent, true)
        );
    },
    update() {
        let data = this.data;
        let rect = this.el.components.flexrect;
        let planes = (this._clippingPlanesLocal = []);
        if (data.clipBottom)
            planes.push(new THREE.Plane(new THREE.Vector3(0, 1, 0), 0));
        if (data.clipTop)
            planes.push(
                new THREE.Plane(new THREE.Vector3(0, -1, 0), rect.height)
            );
        if (data.clipLeft)
            planes.push(new THREE.Plane(new THREE.Vector3(1, 0, 0), 0));
        if (data.clipRight)
            planes.push(
                new THREE.Plane(new THREE.Vector3(-1, 0, 0), rect.width)
            );
        this._clippingPlanes = planes.map((p) => p.clone());
        this._updateMatrix();
    },
    remove() {
        this._filterTargets.forEach((t) =>
            this.el.removeEventListener(t, this._filterEvent, true)
        );
        this._clippingPlanes = [];
        this.applyClippings();
    },
    tick() {
        if (!this.el.object3D.matrixWorld.equals(this._currentMatrix)) {
            this._updateMatrix();
        }
    },
    _filterEvent(ev) {
        if (!(ev.path || ev.composedPath()).includes(this.data.exclude)) {
            if (
                ev.detail.intersection &&
                this.isClipped(ev.detail.intersection.point)
            ) {
                ev.stopPropagation();
                let raycaster =
                    ev.detail.cursorEl &&
                    ev.detail.cursorEl.components.raycaster;
                if (raycaster) {
                    let targets = raycaster.intersectedEls;
                    let c = targets.lastIndexOf(ev.target);
                    if (c >= 0 && c + 1 < targets.length) {
                        targets[c + 1].dispatchEvent(
                            new CustomEvent(ev.type, ev)
                        );
                    }
                }
            }
        }
    },
    _updateMatrix() {
        this._currentMatrix = this.el.object3D.matrixWorld.clone();
        this._clippingPlanesLocal.forEach((plane, i) => {
            this._clippingPlanes[i]
                .copy(plane)
                .applyMatrix4(this._currentMatrix);
        });
        this.applyClippings();
    },
    applyClippings() {
        let excludeObj = this.data.exclude && this.data.exclude.object3D;
        let setCliping = (obj) => {
            if (obj === excludeObj) return;
            if (obj.material && obj.material.clippingPlanes !== undefined) {
                obj.material.clippingPlanes = this._clippingPlanes;
            }
            for (let child of obj.children) {
                setCliping(child);
            }
        };
        setCliping(this.el.object3D);
    },
    isClipped(p) {
        return this._clippingPlanes.some(
            (plane) => plane.distanceToPoint(p) < 0
        );
    },
});

// code from https://github.com/binzume/aframe-flexlayout/blob/master/aframe-flexlayout.js
AFRAME.registerComponent("flexcontainer", {
    dependencies: ["flexrect"],
    schema: {
        spacing: { default: 0.0 },
        padding: { default: 0 },
        reverse: { default: false },
        wrap: { default: "nowrap", oneOf: ["wrap", "nowrap"] },
        direction: {
            default: "column",
            oneOf: ["none", "row", "column", "vertical", "horizontal"],
        },
        alignItems: {
            default: "none",
            oneOf: ["none", "center", "start", "end", "baseline", "stretch"],
        },
        justifyItems: {
            default: "start",
            oneOf: [
                "center",
                "start",
                "end",
                "space-between",
                "space-around",
                "stretch",
            ],
        },
        alignContent: {
            default: "",
            oneOf: ["", "none", "start", "end", "center", "stretch"],
        },
    },
    init() {
        this.el.addEventListener("flexresize", (ev) => this.layout());
        this.layout();
    },
    update() {
        this.layout();
    },
    layout() {
        //console.log("resize");
        let data = this.data;
        let direction = data && data.direction;
        if (!direction || direction == "none") {
            return;
        }
        let containerRect = this.el.components.flexrect,
            children = this.el.children;
        let isVertical = direction == "vertical" || direction == "column";
        let padding = data.padding;
        let spacing = data.spacing;
        let mainDir = data.reverse != isVertical ? -1 : 1;
        let flexmat = isVertical ? [0, 1, mainDir, 0] : [mainDir, 0, 0, -1]; // [main,corss] to [x,y]
        let flexToMainCross = isVertical ? (x, y) => [y, x] : (x, y) => [x, y];
        let containerSize = flexToMainCross(
            containerRect.width - padding * 2,
            containerRect.height - padding * 2
        );
        let attrNames = flexToMainCross("width", "height");

        // lines
        let mainSize = 0;
        let crossSizeSum = 0;
        let lines = [];
        let targets = [];
        let sizeSum = 0;
        let growSum = 0;
        let shrinkSum = 0;
        let crossSize = 0;
        let newLine = () => {
            mainSize = Math.max(
                mainSize,
                sizeSum + spacing * (targets.length - 1)
            );
            crossSizeSum += crossSize;
            lines.push([targets, sizeSum, growSum, shrinkSum, crossSize]);
            targets = [];
            sizeSum = 0;
            growSum = 0;
            shrinkSum = 0;
            crossSize = 0;
        };
        for (let el of children) {
            let flexitem = el.getAttribute("flexitem");
            if (flexitem && flexitem.fixed) {
                continue;
            }
            let rect = el.components.flexrect ||
                el.getAttribute("geometry") || {
                width: (el.getAttribute("width") || undefined) * 1,
                height: (el.getAttribute("height") || undefined) * 1,
            };
            let childScale = el.getAttribute("scale") || { x: 1, y: 1 };
            let pivot = rect.data ? rect.data.pivot : { x: 0.5, y: 0.5 };
            let itemData = {
                el: el,
                flexitem: flexitem,
                size: flexToMainCross(rect.width, rect.height),
                pivot: flexToMainCross(pivot.x, pivot.y),
                scale: flexToMainCross(childScale.x, childScale.y),
            };
            if (itemData.size[0] == null || isNaN(itemData.size[0])) {
                continue;
            }
            let sz = itemData.size[0] * itemData.scale[0];
            let contentSize = sizeSum + sz + spacing * targets.length;
            if (
                data.wrap == "wrap" &&
                sizeSum > 0 &&
                contentSize > containerSize[0]
            ) {
                newLine();
            }
            targets.push(itemData);
            sizeSum += sz;
            growSum += flexitem ? flexitem.grow : 1;
            shrinkSum += flexitem ? flexitem.shrink : 1;
            crossSize =
                itemData.size[1] > crossSize ? itemData.size[1] : crossSize;
        }
        if (targets.length > 0) {
            newLine();
        }

        crossSizeSum += spacing * (lines.length - 1);
        if (containerRect.data[attrNames[0]] == -1) {
            containerSize[0] = mainSize;
            containerRect[attrNames[0]] = mainSize + padding * 2;
        }
        if (containerRect.data[attrNames[1]] == -1) {
            containerSize[1] = crossSizeSum;
            containerRect[attrNames[1]] = crossSizeSum + padding * 2;
        }
        let crossOffset = -containerSize[1] / 2;
        let mainOffset = -containerSize[0] / 2;
        let crossStretch = 0;
        let alignContent = data.alignContent || data.alignItems;
        if (alignContent == "end") {
            crossOffset += containerSize[1] - crossSizeSum;
        } else if (alignContent == "center") {
            crossOffset += (containerSize[1] - crossSizeSum) / 2;
        } else if (alignContent == "stretch" || alignContent == "none") {
            crossStretch = (containerSize[1] - crossSizeSum) / lines.length;
        }
        for (let [targets, sizeSum, growSum, shrinkSum, crossSize] of lines) {
            this._layoutLine(
                targets,
                sizeSum,
                growSum,
                shrinkSum,
                mainOffset,
                crossOffset,
                containerSize[0],
                crossSize + crossStretch,
                flexmat,
                attrNames
            );
            crossOffset += crossSize + crossStretch + spacing;
        }
    },
    /**
     * @param {{el:import("aframe").Entity, flexitem: any, size: number[], pivot: number[], scale:number[]}[]} targets
     * @param {number} sizeSum
     * @param {number} growSum
     * @param {number} shrinkSum
     * @param {number} p
     * @param {number} crossOffset
     * @param {number} containerSize0
     * @param {number} containerSize1
     * @param {number[]} flexmat
     * @param {string[]} attrNames
     */
    _layoutLine(
        targets,
        sizeSum,
        growSum,
        shrinkSum,
        p,
        crossOffset,
        containerSize0,
        containerSize1,
        flexmat,
        attrNames
    ) {
        let { justifyItems, alignItems, spacing, wrap } = this.data;
        let stretchFactor = 0;
        let numTarget = targets.length;
        if (justifyItems === "center") {
            p += (containerSize0 - sizeSum - spacing * numTarget) / 2;
        } else if (justifyItems === "end") {
            p += containerSize0 - sizeSum - spacing * numTarget;
        } else if (justifyItems === "stretch") {
            stretchFactor =
                containerSize0 - sizeSum - spacing * (numTarget - 1);
            if (stretchFactor > 0) {
                stretchFactor = growSum > 0 ? stretchFactor / growSum : 0;
            } else {
                stretchFactor = shrinkSum > 0 ? stretchFactor / shrinkSum : 0;
            }
        } else if (justifyItems === "space-between") {
            spacing = (containerSize0 - sizeSum) / (numTarget - 1);
        } else if (justifyItems === "space-around") {
            spacing = (containerSize0 - sizeSum) / numTarget;
            p += spacing / 2;
        }

        for (let itemData of targets) {
            let el = itemData.el;
            let flexitem = itemData.flexitem;
            let [pivot0, pivot1] = itemData.pivot;
            let [scale0, scale1] = itemData.scale;
            let [size0, size1] = itemData.size;
            let align = (flexitem && flexitem.align) || alignItems;
            let stretch =
                (flexitem
                    ? stretchFactor > 0
                        ? flexitem.grow
                        : flexitem.shrink
                    : 1) * stretchFactor;
            let szMain = size0 * scale0 + stretch;
            let posMain = p + pivot0 * szMain;
            let posCross = crossOffset + containerSize1 / 2; // center
            let pos = el.getAttribute("position") || { x: 0, y: 0, z: 0 };
            if (scale0 > 0 && stretch != 0) {
                el.setAttribute(attrNames[0], size0 + stretch / scale0);
            }
            if (scale1 > 0 && align === "stretch") {
                size1 = containerSize1;
                el.setAttribute(attrNames[1], size1 / scale1);
            }
            if (align === "start" || align === "stretch") {
                posCross = crossOffset + pivot1 * size1;
            } else if (align === "end") {
                posCross = crossOffset + containerSize1 - (1 - pivot1) * size1;
            } else if (align === "center") {
                posCross += (pivot1 - 0.5) * size1;
            } else if (align === "none" && wrap != "wrap") {
                // Keep original cross position if nowrap.
                posCross += flexmat[1] * pos.x + flexmat[3] * pos.y;
            }
            pos.x = flexmat[0] * posMain + flexmat[1] * posCross;
            pos.y = flexmat[2] * posMain + flexmat[3] * posCross;
            el.setAttribute("position", pos);
            p += szMain + spacing;
        }
    },
});

AFRAME.registerComponent("flexitem", {
    schema: {
        align: {
            default: "none",
            oneOf: ["none", "center", "start", "end", "baseline", "stretch"],
        },
        grow: { default: 1 },
        shrink: { default: 1 },
        fixed: { default: false },
    },
    update(oldData) {
        if (oldData.align !== undefined) {
            let flexcontainer = this.el.parentNode.components.flexcontainer;
            if (flexcontainer) {
                flexcontainer.layout();
            }
        }
    },
});

AFRAME.registerComponent("flexrect", {
    dependencies: ["troika-text"],
    schema: {
        width: { default: -1 }, // -1 : auto
        height: { default: -1 },
        pivot: { type: "vec2", default: { x: 0.5, y: 0.5 } },
        updateGeometry: { default: false },
    },
    init() {
        //try init troyka
        let troika = this.el.components["troika-text"] || {};
        if (troika) {
            //if troika is present, then we try to set it up
            //it wont be ready directly, so we set variables to be made
            this.troika = troika;
            this.troikaReady = false;
            //console.log("initing troika");
            this.BoundingBox = new THREE.Box3();
            this.BoundingBox.copy(
                troika.troikaTextMesh.geometry.boundingBox
            ).applyMatrix4(troika.troikaTextMesh.matrixWorld);
            //console.log(this.BoundingBox);
        }
    },
    update(oldData) {
        //console.log("update");
        let el = this.el;
        let { width, height, updateGeometry } = this.data;
        let geometry = el.getAttribute("geometry") || {};
        let rounded = el.getAttribute("rounded") || {}; //Add to rounded as a geometry reference

        if (this.troika && this.troikaReady) {
            // We look deep inside troika mesh geometry to find our text size
            //console.log(this);

            let troikaBB =
                this.troika.troikaTextMesh.geometry.boundingBox || {};
            //console.log(troikaBB.max);
            // console.log(troikaBB,troikaBB.max.x, troikaBB.min.x);

            this.troika.width = troikaBB.max.x - troikaBB.min.x;
            this.troika.height = troikaBB.max.y - troikaBB.min.y;
            //console.log(troika.troikaTextMesh.geometry.boundingBox.containsBox());
        }

        this.width =
            width < 0
                ? (el.getAttribute("width") ||
                    geometry.width ||
                    rounded.width ||
                    this.troika.width ||
                    0) * 1
                : width;
        this.height =
            height < 0
                ? (el.getAttribute("height") ||
                    geometry.height ||
                    rounded.height ||
                    this.troika.height ||
                    0) * 1
                : height;
        if (oldData.width !== undefined) {
            if (updateGeometry) {
                el.setAttribute("geometry", { width: width, height: height });
            }
            el.parentEl.emit("flexresize", { flexrect: this }, false);
        }
    },
    tick() {
        if (this.troika && !this.troikaReady) {
            this.troika.troikaTextMesh.geometry.computeBoundingBox();
            //this.BoundingBox.copy( troika.troikaTextMesh.geometry.boundingBox ).applyMatrix4( troika.troikaTextMesh.matrixWorld );
            //console.log(this.troika.troikaTextMesh.geometry.boundingBox.max.y);
            if (
                isFinite(this.troika.troikaTextMesh.geometry.boundingBox.max.y)
            ) {
                //console.log("ready");
                this.troikaReady = true;
                this.el.setAttribute("flexrect", {
                    width: -1.1,
                    height: -1.1,
                });
            }
        }
    },
});

AFRAME.registerPrimitive("a-flexcontainer", {
    defaultComponents: {
        flexrect: {},
        flexcontainer: {},
    },
    mappings: {
        width: "flexrect.width",
        height: "flexrect.height",
        flpivot: "flexrect.pivot",
        direction: "flexcontainer.direction",
        spacing: "flexcontainer.spacing",
        padding: "flexcontainer.padding",
        reverse: "flexcontainer.reverse",
        wrap: "flexcontainer.wrap",
        "align-items": "flexcontainer.alignItems",
        "justify-items": "flexcontainer.justifyItems",
        "align-content": "flexcontainer.alignContent",
    },
});
