AFRAME.registerComponent("filter-text", {
    dependencies: ['flexrect', 'troika-text', 'text'],
    schema: {
        value: { default: "" },
        targetProperty: { default: "value" },
        targetComponent: { default: "troika-text" },
        escape: { default: false },
        unescape: { default: false },
        lower: { default: false },
        upper: { default: false },
        siteOnly: { default: false },
        rootDomainOnly: { default: false },
        noHttp: { default: false },
        trim: { default: true },
        truncateOmission: { default: "..." },
        truncateLength: { default: -1 },
        debug: { default: false }
    },
    update(oldData) {
        const d = this.data;
        //console.log(d);
        let string = d.value;
        const targetProperty = d.targetProperty;
        const targetComponent = d.targetComponent;

        //Apply styles

        if (d.escape)
            string = _.escape(string);
        //https://lodash.com/docs/4.17.15#escape

        if (d.unescape)
            string = _.unescape(string);
        //https://lodash.com/docs/4.17.15#unescape

        if (d.lower)
            string = _.toLower(string);
        //https://lodash.com/docs/4.17.15#toLower

        if (d.upper)
            string = _.toUpper(string);
        //https://lodash.com/docs/4.17.15#toUpper

        if (d.trim)
            string = _.trim(string);
        //https://lodash.com/docs/4.17.15#trim



        if (d.debug) {
            //console.log(d, "original string:" + d.value + "\nnew value:" + string);
            console.log(this, this.el);
            console.log(targetComponent);
            console.log(targetProperty);
            console.log(string);
        }

        if (d.noHttp) {
            string = _.replace(string, "https://", "");
            string = _.replace(string, "http://", "");
        }

        if (d.siteOnly) {
            string = _.replace(string, "https://", "");
            string = _.replace(string, "http://", "");
            string = string.split(/[/?#]/)[0];
        }

        if (d.rootDomainOnly) {
            string = _.replace(string, "https://", "");
            string = _.replace(string, "http://", "");
            string = string.split(/[/?#]/)[0];
            splitUrl = string.split(".");
            string = splitUrl[splitUrl.length - 2] + "." + splitUrl[splitUrl.length - 1];
        }

        if (d.truncateLength > 0)
            string = _.truncate(string, {
                'omission': d.truncateOmission,
                'length': d.truncateLength
            })
        //https://lodash.com/docs/4.17.15#truncate


        this.el.setAttribute(targetComponent, targetProperty, string);

        this.forceUpdate = true;


    },
    tick: function () {
        //console.log("tick");

        if (!this.forceUpdate)
            return;

        //In case of flexrect use, it might not update, soo let's update it ourself after Update is called
        let flexrect = this.el.components.flexrect;
        //console.log(flexrect);
        if (flexrect) {
            //console.log(this);
            this.el.setAttribute("flexrect", "height", flexrect.height + 0.01);
            //console.log("ask for resize");
            let flexcontainer = this.el.parentNode.components.flexcontainer;
            if (flexcontainer)
                flexcontainer.layout();
            //this.el.parentEl.emit("flexresize", { flexrect: this }, false);
        }

        this.forceUpdate = false;
    }
})