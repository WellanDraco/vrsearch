AFRAME.registerComponent("button-template", {
    schema: {
        class: { default: "button" },
        roundedRadius: { default: 0.015 },
        roundedOpacity: { default: 0.75 },
        roundedColor: { type: "color", default: "#e6e3e8" },
        roundedHeight: { default: 0.1 },
        roundedWidth: { default: 0.15 },
        roundedPosition: {
            type: "vec3",
            default: { x: -0.075, y: -0.05, z: 0 },
        },
        imageSrc: { default: "" },
        imageScale: { default: 0.1 },
        imagePosition: {
            type: "vec3",
            default: { x: 0.075, y: 0.05, z: 0.001 },
        },
        imageMaterialAlphaTest: { default: 0.5 },
        src: {type:'asset', default:'templates/button-template.handlebars'},
        type: {default:'handlebars'}
    },
    init(){
        //template="data:button-template; src:templates/button-template.handlebars; type:handlebars"
        this.el.setAttribute('template',{
            data:'button-template',
            src: this.data.src,
            type: this.data.type
        })
    }
});
