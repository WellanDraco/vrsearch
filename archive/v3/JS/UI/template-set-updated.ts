//Update of the template-set component
AFRAME.registerComponent("template-set-updated", {
    schema: {
        on: { type: "string" },
        src: { type: "string" },
        data: { type: "string" },
        type: { type: "string" },
    },

    init: function () {
        var data = this.data;
        var el = this.el;
        el.addEventListener(data.on, function () {
            el.setAttribute("template", {
                src: data.src,
                data: data.data,
                type: data.type,
            });
        });
    },
});
