AFRAME.registerComponent("result-panel-template", {
    schema: {
        resultData: { 
            default:'{}',
            parse: JSON.parse,
            stringify: JSON.stringify
          },
        resultType: {
            default: "media"
        },
        src: { type: "asset", default: "templates\\result-template.handlebars" },
        type: { default: "handlebars" },
    },
    init() {
        this.el.setAttribute("template-set-updated", {
            on: "triggerRender",
            data: "result-panel-template",
            src: this.data.src,
            type: this.data.type,
        });

    },
    update(oldData){

        //On récupère la liste des données pour notre query
        let queries = this.el.sceneEl.systems.state.state.queries;
        let index = _.findIndex(queries, { 'name': this.data.resultType});

        //On ne garde que les 9 premiers éléments (min inclus, max exclus)
        this.data.resultData =  _.slice(queries[index].value, 0, 9);
        //console.log(this.data.resultData );
        if(!_.isEmpty(this.data.resultData)){
            //console.log("valid");
            this.el.emit("triggerRender");
        }
    }
});
