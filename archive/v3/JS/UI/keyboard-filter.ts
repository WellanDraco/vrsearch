AFRAME.registerComponent('keyboard-filter', {
    init() {

        this.keyboard = this.el.components['super-keyboard'];
        //console.log(this);
        //L'affichage semble afficher de mauvais caractères, donc on nettoie les inputs utilisateur
        this.keyboard.setCustomFilter(function (str) {
            //console.log(this);
            //console.log(str);
            return str;
        });
        this.bindMethods();
    },
    pause() {
        document.removeEventListener('keydown', this.keydown);
    },
    play() {
        document.addEventListener('keydown', this.keydown);
    },
    keydown(e){
        const accepted = (e.key.length == 1) ? true : false;
        //console.log(e.key,this.keyboard);
        var key = e.key;
        if(key == "Backspace" || key == "Enter"){
            e.preventDefault();
        }
        key = (key != "Backspace")? key : "Delete";

        this.keyboard.hover();
        var keys = this.keyboard.KEYBOARDS[this.keyboard.data.model].layout;
        for (var i = 0; i < keys.length; i++) {
            var k = keys[i];
            if (!k.el) continue;

            //Bbbconsole.log(k.key, key);
            if (k.key === key) {
                if (this.keyboard.keyHover && (this.keyboard.keyHover.key != 'Shift' || !this.keyboard.shift)) {
                    this.keyboard.keyHover.el.material.color.set(this.keyboard.keyBgColor);
                }
                k.el.material.color.set(this.keyboard.keyHoverColor);
                this.keyboard.keyHover = k;
                break;
            }
        }
        this.keyboard.click();
        this.keyboard.blur();
    },
    bindMethods: function () {
        this.keydown = this.keydown.bind(this);
    },
  });