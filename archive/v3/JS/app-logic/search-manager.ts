
const fakeresultMixed = [
    {
        "BingAPIs-TraceId": "6C91E991E5DA4E4B8AEF453960E8F311",
        "BingAPIs-SessionId": "21E1CC7382C74E8CB0C657D6428C01E2",
        "X-MSEdge-ClientID": "12F12E31F18B6DBE1F3521BEF0926C2D",
        "BingAPIs-Market": "fr-FR",
        "X-MSEdge-Ref":
            "Ref A: 6C91E991E5DA4E4B8AEF453960E8F311 Ref B: AMS04EDGE0512 Ref C: 2020-11-27T21:29:35Z",
    },
    {
        _type: "SearchResponse",
        queryContext: {
            originalQuery: "bonjour",
            askUserForLocation: true,
        },
        webPages: {
            webSearchUrl: "https:\/\/www.bing.com\/search?q=coucou",
            totalEstimatedMatches: 4820000,
            value: [
                {
                    "id": "https:\/\/vr-search-engine.cognitiveservices.azure.com\/api\/v7\/#WebPages.0",
                    "contractualRules": [
                        {
                            "_type": "ContractualRules\/LicenseAttribution",
                            "targetPropertyName": "snippet",
                            "targetPropertyIndex": 0,
                            "mustBeCloseToContent": true,
                            "license": {
                                "name": "CC-BY-SA",
                                "url": "http:\/\/creativecommons.org\/licenses\/by-sa\/3.0\/"
                            },
                            "licenseNotice": "Texte sous licence CC-BY-SA"
                        }],
                    "name": "Coucou \u2014 Wikip\u00e9dia",
                    "url": "https:\/\/fr.wikipedia.org\/wiki\/Coucou",
                    "isFamilyFriendly": true,
                    "displayUrl": "https:\/\/fr.wikipedia.org\/wiki\/Coucou",
                    "snippet": "Coucou dans la culture. Les coucous, souvent entendus et rarement vus, sont annonciateurs du printemps car leur chant retentit clairement dans les for\u00eats marquant le d\u00e9but de la belle saison [5].. Les coucous sont connus pour ne pas construire de nid, m\u00eame si ce n'est pas toujours le cas.",
                    "dateLastCrawled": "2021-02-12T15:12:00.0000000Z",
                    "language": "fr",
                    "isNavigational": false
                }, {
                    "id": "https:\/\/vr-search-engine.cognitiveservices.azure.com\/api\/v7\/#WebPages.1",
                    "name": "D\u00e9finitions : coucou - Dictionnaire de fran\u00e7ais Larousse",
                    "url": "https:\/\/www.larousse.fr\/dictionnaires\/francais\/coucou\/19705",
                    "isFamilyFriendly": true,
                    "displayUrl": "https:\/\/www.larousse.fr\/dictionnaires\/francais\/coucou",
                    "snippet": "D\u00e9finitions de coucou. Gros oiseau passereau, insectivore et migrateur, connu pour son chant printanier caract\u00e9ristique et ses m\u0153urs parasites. (La femelle pond chacun de ses \u0153ufs dans un nid de passereaux d'une autre esp\u00e8ce, aux \u0153ufs de couleur voisine des siens ; les parents adoptifs couvent, puis nourrissent le jeune coucou, qui jette les \u0153ufs de l'esp\u00e8ce-h\u00f4te par-dessus bord.)",
                    "dateLastCrawled": "2021-02-11T22:14:00.0000000Z",
                    "language": "fr",
                    "isNavigational": false
                }, {
                    "id": "https:\/\/vr-search-engine.cognitiveservices.azure.com\/api\/v7\/#WebPages.2",
                    "name": "coucou \u2014 Wiktionnaire", "url": "https:\/\/fr.wiktionary.org\/wiki\/coucou",
                    "isFamilyFriendly": true,
                    "displayUrl": "https:\/\/fr.wiktionary.org\/wiki\/coucou",
                    "snippet": "coucou \\ku.ku\\ masculin (Ornithologie) Nom normalis\u00e9 de 11 genres assez h\u00e9t\u00e9rog\u00e8nes comprenant 56 esp\u00e8ces d'oiseaux de la famille des cuculid\u00e9s habitant l'Ancien Monde, essentiellement caract\u00e9ris\u00e9es par leur parasitisme de couv\u00e9e, habitude par laquelle elles confient la couvaison et l'\u00e9levage des oisillons \u00e0 d'autres esp\u00e8ces d'oiseaux le plus souvent plus petites qu'elles; l ...",
                    "dateLastCrawled": "2021-02-12T08:01:00.0000000Z",
                    "language": "fr",
                    "isNavigational": false
                }, {
                    "id": "https:\/\/vr-search-engine.cognitiveservices.azure.com\/api\/v7\/#WebPages.3",
                    "name": "D\u00e9finition coucou | Dictionnaire fran\u00e7ais | Reverso",
                    "url": "https:\/\/dictionnaire.reverso.net\/francais-definition\/coucou",
                    "isFamilyFriendly": true,
                    "displayUrl": "https:\/\/dictionnaire.reverso.net\/francais-definition\/coucou",
                    "snippet": "Cherchez coucou et beaucoup d\u2019autres mots dans le dictionnaire de d\u00e9finition et synonymes fran\u00e7ais de Reverso. Vous pouvez compl\u00e9ter la d\u00e9finition de coucou propos\u00e9e par le dictionnaire de fran\u00e7ais Reverso en consultant d\u2019autres dictionnaires sp\u00e9cialis\u00e9s dans la d\u00e9finition de mots fran\u00e7ais : Wikipedia, Tr\u00e9sor de la langue fran\u00e7aise, Lexilogos, dictionnaire Larousse, Le Robert ...",
                    "dateLastCrawled": "2021-02-11T18:21:00.0000000Z",
                    "language": "fr",
                    "isNavigational": false
                }, {
                    "id": "https:\/\/vr-search-engine.cognitiveservices.azure.com\/api\/v7\/#WebPages.4",
                    "name": "Coucou gris (Cuculus canorus), l\u2019oiseau voleur de nid",
                    "url": "https:\/\/jardinage.lemonde.fr\/dossier-1185-coucou-gris-cuculus-canorus-oiseau-voleur-nid.html",
                    "isFamilyFriendly": true, "displayUrl": "https:\/\/jardinage.lemonde.fr\/dossier-1185-coucou-gris-cuculus-canorus-oiseau-voleur...", "snippet": "Le coucou gris (Cuculus canorus) est un oiseau de taille moyenne qui ressemble \u00e0 certains petits rapaces.Il appartient \u00e0 l\u2019ordre des cuculiformes. Son chant, qui lui donne son nom, est familier au printemps dans les campagnes mais aussi dans les jardins des villes.. Comment reconna\u00eetre un coucou ?", "dateLastCrawled": "2021-02-11T02:04:00.0000000Z", "language": "fr", "isNavigational": false
                }, {
                    "id": "https:\/\/vr-search-engine.cognitiveservices.azure.com\/api\/v7\/#WebPages.5", "name": "Coucou gris - Cuculus canorus - oiseaux", "url": "https:\/\/www.oiseaux.net\/oiseaux\/coucou.gris.html", "isFamilyFriendly": true, "displayUrl": "https:\/\/www.oiseaux.net\/oiseaux\/coucou.gris.html", "snippet": "Le chant du Coucou gris m\u00e2le est le \"cou cou\" bien connu, de tonalit\u00e9 \u00e9lev\u00e9e, la premi\u00e8re note note un peu plus haute que la seconde. Ce chant est \u00e9mis presque bec ferm\u00e9, celui-ci s'entrouvrant un peu pour la premi\u00e8re note. Il arrive que la premi\u00e8re note soit doubl\u00e9e dans l'excitation \"cou cou cou\". La phrase se termine parfois par une suite de 3 ou 4 notes dures \"Hah hah hah hah\".", "dateLastCrawled": "2021-02-12T11:26:00.0000000Z", "language": "fr", "isNavigational": false
                }, {
                    "id": "https:\/\/vr-search-engine.cognitiveservices.azure.com\/api\/v7\/#WebPages.6", "name": "Coucous - La maison de la pendule", "url": "https:\/\/www.maison-pendule.fr\/pendule-coucou-_l_FR_r_165_va_1.html", "isFamilyFriendly": true, "displayUrl": "https:\/\/www.maison-pendule.fr\/pendule-coucou-_l_FR_r_165_va_1.html", "snippet": "Vente de coucou en ligne, que ce soit des horloges coucous ou des coucous de la foret noire, ils sont tous livr\u00e9s avec un certificat d\u2019authenticit\u00e9. Nous proposons des coucous m\u00e9caniques traditio", "dateLastCrawled": "2021-02-09T00:57:00.0000000Z", "language": "fr", "isNavigational": false
                }, {
                    "id": "https:\/\/vr-search-engine.cognitiveservices.azure.com\/api\/v7\/#WebPages.7", "name": "Coucou, des amis pour tous les petits!", "url": "https:\/\/coucou.telequebec.tv\/", "isFamilyFriendly": true, "displayUrl": "https:\/\/coucou.telequebec.tv", "snippet": "Coucou de T\u00e9l\u00e9-Qu\u00e9bec est un univers immersif destin\u00e9 aux petits. Il marque les moments-cl\u00e9s de leur journ\u00e9e et les accompagne dans leur routine. En plus des six coucous, les enfants trouveront sur cette plateforme du contenu con\u00e7u et pens\u00e9 pour eux : \u00e9missions, jeux, histoires et plus encore!", "dateLastCrawled": "2021-02-12T08:13:00.0000000Z", "language": "fr", "isNavigational": false
                }, {
                    "id": "https:\/\/vr-search-engine.cognitiveservices.azure.com\/api\/v7\/#WebPages.8", "name": "La boutique en ligne des pendules ... - Pendule-a-coucou.com", "url": "https:\/\/www.pendule-a-coucou.com\/", "isFamilyFriendly": true, "displayUrl": "https:\/\/www.pendule-a-coucou.com", "snippet": "La pendule \u00e0 coucou a \u00e9t\u00e9 invent\u00e9e en 1737 dans notre village de Sch\u00f6nwald, dans la For\u00eat-Noire en Allemagne. Tous nos coucous sont exclusivement fabriqu\u00e9s dans la For\u00eat-Noire - regardez la vid\u00e9o vid\u00e9o. Tous nos coucous m\u00e9caniques sont certifi\u00e9s par l\u2019Association Coucou For\u00eat Noire (VdS). Tous nos coucous sont garantis jusqu\u2019\u00e0 5 ans, nous avons r\u00e9seaux services mondial. Nous ...", "dateLastCrawled": "2021-02-11T21:24:00.0000000Z", "language": "fr", "isNavigational": false
                }
            ]
        },
        entities: {
            value: [
                {
                    id:
                        "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#Entities.0",
                    contractualRules: [
                        {
                            _type: "ContractualRules/LicenseAttribution",
                            targetPropertyName: "description",
                            mustBeCloseToContent: true,
                            license: {
                                name: "CC-BY-SA",
                                url:
                                    "http://creativecommons.org/licenses/by-sa/3.0/",
                            },
                            licenseNotice: "Texte sous licence CC-BY-SA",
                        },
                        {
                            _type: "ContractualRules/LinkAttribution",
                            targetPropertyName: "description",
                            mustBeCloseToContent: true,
                            text: "Wikipedia",
                            url: "http://fr.wikipedia.org/wiki/All%C3%B4",
                        },
                        {
                            _type: "ContractualRules/MediaAttribution",
                            targetPropertyName: "image",
                            mustBeCloseToContent: true,
                            url: "http://fr.wikipedia.org/wiki/All%C3%B4",
                        },
                    ],
                    webSearchUrl:
                        "https://www.bing.com/entityexplore?q=All%C3%B4&filters=sid:%228a38d74b-6cac-f4f9-4c5a-29fc13f0d1d0%22&elv=AXXfrEiqqD9r3GuelwApulpIQKhSEbZESMnLIlmAuSab2u47nNf!0sO88rN!a0GJFyaMqVJirFp06elIqZaRfI1tiQ4jpcHLWkB7PJAmOt99",
                    name: "All\u00f4",
                    image: {
                        name: "All\u00f4",
                        thumbnailUrl:
                            "https://www.bing.com/th?id=AMMS_23d0b6f6f5726ce00c49ae036d65dbb7&w=110&h=110&c=7&rs=1&qlt=80&cdv=1&pid=16.1",
                        provider: [
                            {
                                _type: "Organization",
                                url: "http://fr.wikipedia.org/wiki/All%C3%B4",
                            },
                        ],
                        hostPageUrl:
                            "http://upload.wikimedia.org/wikipedia/commons/9/93/Gustave_Courbet_-_Bonjour_Monsieur_Courbet_-_Mus%C3%A9e_Fabre.jpg",
                        width: 110,
                        height: 110,
                        sourceWidth: 474,
                        sourceHeight: 410,
                    },
                    description:
                        "Bonjour est la salutation la plus commun\u00e9ment employ\u00e9e en fran\u00e7ais lorsque l'on rencontre ou croise une connaissance, ou une personne inconnue dans le cadre d'une pr\u00e9sentation, et ce, du matin jusqu'\u00e0 la fin de la journ\u00e9e. Par extension, \u00ab bonjour \u00bb est employ\u00e9 \u00e9galement pour les rencontres virtuelles telles que la correspondance, les t\u00e9l\u00e9communications ou les m\u00e9dias. Prononcer \u00ab bonjour \u00bb en guise de salutation est une marque de politesse.",
                    entityPresentationInfo: {
                        entityScenario: "DominantEntity",
                        entityTypeHints: ["Generic"],
                    },
                    bingId: "8a38d74b-6cac-f4f9-4c5a-29fc13f0d1d0",
                },
            ],
        },
        news: {
            id:
                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#News",
            readLink:
                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/news/search?q=bonjour",
            value: [
                {
                    contractualRules: [
                        {
                            _type: "ContractualRules/TextAttribution",
                            text: "superprof.fr",
                        },
                    ],
                    name:
                        "Bonjour! Fran\u00e7aise d'origine, exp\u00e9rience de cours particuliers et cours scolaires pour \u00e9trangers. Tous niveaux.",
                    url:
                        "https://www.superprof.fr/francaise-origine-experience-cours-particuliers-cours-scolaires-etrangers-niveaux.html",
                    image: {
                        contentUrl:
                            "https://www.superprof.fr/images/annonces/professeur-home-francaise-origine-experience-cours-particuliers-cours-scolaires-etrangers-niveaux.jpg",
                        thumbnail: {
                            contentUrl:
                                "https://www.bing.com/th?id=OVFT.0MzCue2-IzrnQQzuzo8LpC&pid=News",
                            width: 300,
                            height: 300,
                        },
                    },
                    provider: [
                        {
                            _type: "Organization",
                            name: "superprof.fr",
                        },
                    ],
                    datePublished: "2020-11-27T06:15:00.0000000Z",
                },
                {
                    contractualRules: [
                        {
                            _type: "ContractualRules/TextAttribution",
                            text: "Ladepeche.fr",
                        },
                    ],
                    name:
                        "Toulouse. Test antig\u00e9nique : un trait, \u00e7a va, deux traits\u2026 bonjour le coronavirus",
                    url:
                        "https://www.ladepeche.fr/2020/11/25/test-antigenique-un-trait-ca-va-deux-traits-bonjour-le-coronavirus-9219178.php",
                    image: {
                        contentUrl:
                            "https://images.ladepeche.fr/api/v1/images/view/5fbdd9958fe56f41777f5c05/large/image.jpg?v=1",
                        thumbnail: {
                            contentUrl:
                                "https://www.bing.com/th?id=OVFT.pU8YpCZXTceMGparlcAkOi&pid=News",
                            width: 700,
                            height: 395,
                        },
                    },
                    about: [
                        {
                            readLink:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/entities/66f18335-714d-a89b-accf-daa50ae2a3b0",
                            name: "Toulouse",
                        },
                    ],
                    provider: [
                        {
                            _type: "Organization",
                            name: "Ladepeche.fr",
                        },
                    ],
                    datePublished: "2020-11-25T03:09:00.0000000Z",
                },
                {
                    contractualRules: [
                        {
                            _type: "ContractualRules/TextAttribution",
                            text: "allocine.fr",
                        },
                    ],
                    name: "Fini l'Enfant, bonjour Grogu",
                    url:
                        "https://www.allocine.fr/diaporamas/series/diaporama-18694921/?page=9",
                    image: {
                        contentUrl:
                            "https://fr.web.img6.acsta.net/newsv7/20/11/27/14/44/5502907.jpg",
                        thumbnail: {
                            contentUrl:
                                "https://www.bing.com/th?id=OVFT.CaqLj8k8JqLas-PV9P024S&pid=News",
                            width: 700,
                            height: 393,
                        },
                    },
                    description:
                        'D\u00e9couvrez les easter eggs du chapitre 13 de "The Mandalorian" (ou le cinqui\u00e8me \u00e9pisode de la saison 2), intitul\u00e9 "La Jedi" et disponible depuis ce vendredi 27 novembre sur Disney+ - ATTENTION SPOILERS',
                    provider: [
                        {
                            _type: "Organization",
                            name: "allocine.fr",
                        },
                    ],
                    datePublished: "2020-11-27T14:41:00.0000000Z",
                },
                {
                    contractualRules: [
                        {
                            _type: "ContractualRules/TextAttribution",
                            text: "L'Est r\u00e9publicain",
                        },
                    ],
                    name:
                        "Besan\u00e7on | N\u00e9crologie Josette Bonjour n\u2019est plus",
                    url:
                        "https://www.estrepublicain.fr/societe/2020/11/23/josette-bonjour-n-est-plus",
                    image: {
                        contentUrl:
                            "https://cdn-s-www.estrepublicain.fr/images/FC62FB4C-4B11-46BE-AD03-AF506535AA60/FB1200/photo-1606152122.jpg",
                        thumbnail: {
                            contentUrl:
                                "https://www.bing.com/th?id=OVFT.BPN8XhkA2tkyEQOCxhFIhS&pid=News",
                            width: 700,
                            height: 367,
                        },
                    },
                    about: [
                        {
                            readLink:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/entities/5dc2ab9b-b196-0753-49d8-a3219a51ccaa",
                            name: "Besan\u00e7on",
                        },
                        {
                            readLink:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/entities/8b225af5-3a37-5495-7a2e-0cc4bd97393d",
                            name: "Josette du Pres",
                        },
                    ],
                    provider: [
                        {
                            _type: "Organization",
                            name: "L'Est r\u00e9publicain",
                        },
                    ],
                    datePublished: "2020-11-25T13:21:00.0000000Z",
                    category: "France",
                },
                {
                    contractualRules: [
                        {
                            _type: "ContractualRules/TextAttribution",
                            text: "Metrotime via MSN.com",
                        },
                    ],
                    name:
                        "Le coronavirus va-t-il sonner la fin de la bise pour se dire bonjour",
                    url:
                        "https://www.msn.com/fr-be/actualite/other/le-coronavirus-va-t-il-sonner-la-fin-de-la-bise-pour-se-dire-bonjour/ar-BB1bougW",
                    image: {
                        contentUrl:
                            "https://img-s-msn-com.akamaized.net/tenant/amp/entityid/BB1boAf3.img?h=315&w=600&m=6&q=60&o=t&l=f&f=jpg&x=449&y=293",
                        thumbnail: {
                            contentUrl:
                                "https://www.bing.com/th?id=OVFT.hMFECGGS6YMiz4sxAZM3Sy&pid=News",
                            width: 600,
                            height: 315,
                        },
                    },
                    description:
                        "La pand\u00e9mie de coronavirus va-t-elle changer nos habitudes ? Dans le monde d\u2019apr\u00e8s, va-t-on arr\u00eater de se faire la bise pour se dire bonjour ou au revoir ? Pas s\u00fbr ! Depuis l\u2019apparition du coronavirus",
                    mentions: [
                        {
                            name: "Le",
                        },
                        {
                            name: "Bise",
                        },
                        {
                            name: "Ne",
                        },
                    ],
                    provider: [
                        {
                            _type: "Organization",
                            name: "Metrotime via MSN.com",
                        },
                    ],
                    datePublished: "2020-11-26T21:00:05.0000000Z",
                },
            ],
        },
        places: {
            value: [
                {
                    _type: "LocalBusiness",
                    id:
                        "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#Places.0",
                    entityPresentationInfo: {
                        entityScenario: "ListItem",
                    },
                },
                {
                    _type: "LocalBusiness",
                    id:
                        "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#Places.1",
                    entityPresentationInfo: {
                        entityScenario: "ListItem",
                    },
                },
                {
                    _type: "LocalBusiness",
                    id:
                        "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#Places.2",
                    entityPresentationInfo: {
                        entityScenario: "ListItem",
                    },
                },
            ],
        },
        videos: {
            id:
                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#Videos",
            readLink:
                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/videos/search?q=bonjour",
            webSearchUrl: "https://www.bing.com/videos/search?q=bonjour",
            isFamilyFriendly: true,
            value: [
                {
                    webSearchUrl:
                        "https://www.bing.com/videos/search?q=bonjour&view=detail&mid=420FAB4A8C06964BCC09420FAB4A8C06964BCC09",
                    name: "RTI - Bonjour 2019",
                    thumbnailUrl:
                        "https://tse3.mm.bing.net/th?id=OVP.432PvmTP9AuAXo5iD1p9XgEsDh&pid=Api",
                    datePublished: "2020-01-07T08:44:20.0000000",
                    publisher: [
                        {
                            name: "YouTube",
                        },
                    ],
                    isAccessibleForFree: true,
                    contentUrl: "https://www.youtube.com/watch?v=JqCTdsr_U9I",
                    hostPageUrl: "https://www.youtube.com/watch?v=JqCTdsr_U9I",
                    encodingFormat: "mp4",
                    hostPageDisplayUrl:
                        "https://www.youtube.com/watch?v=JqCTdsr_U9I",
                    width: 1280,
                    height: 720,
                    duration: "PT1H55M53S",
                    embedHtml:
                        '<iframe width="1280" height="720" src="http://www.youtube.com/embed/JqCTdsr_U9I?autoplay=1" frameborder="0" allowfullscreen></iframe>',
                    allowHttpsEmbed: true,
                    viewCount: 489141,
                    thumbnail: {
                        width: 160,
                        height: 120,
                    },
                    allowMobileEmbed: true,
                    isSuperfresh: false,
                },
                {
                    webSearchUrl:
                        "https://www.bing.com/videos/search?q=bonjour&view=detail&mid=86066907D1E02FEC47AF86066907D1E02FEC47AF",
                    name:
                        "[Bonjour 2019] - Le show de Agalawal \u00e0 Dimbokro",
                    thumbnailUrl:
                        "https://tse1.mm.bing.net/th?id=OVP.FnhTDVBPGrSW6qFVmOmPVQEsDh&pid=Api",
                    datePublished: "2019-02-07T08:00:00.0000000",
                    publisher: [
                        {
                            name: "YouTube",
                        },
                    ],
                    isAccessibleForFree: true,
                    contentUrl: "https://www.youtube.com/watch?v=dbUv30mFl44",
                    hostPageUrl: "https://www.youtube.com/watch?v=dbUv30mFl44",
                    encodingFormat: "mp4",
                    hostPageDisplayUrl:
                        "https://www.youtube.com/watch?v=dbUv30mFl44",
                    width: 1280,
                    height: 720,
                    duration: "PT25M34S",
                    embedHtml:
                        '<iframe width="1280" height="720" src="http://www.youtube.com/embed/dbUv30mFl44?autoplay=1" frameborder="0" allowfullscreen></iframe>',
                    allowHttpsEmbed: true,
                    viewCount: 1914120,
                    thumbnail: {
                        width: 160,
                        height: 120,
                    },
                    allowMobileEmbed: true,
                    isSuperfresh: false,
                },
                {
                    webSearchUrl:
                        "https://www.bing.com/videos/search?q=bonjour&view=detail&mid=0764B6343C2793C617970764B6343C2793C61797",
                    name:
                        "Apprendre le fran\u00e7ais/Chanson pour enfants/Bonjour mes amis/French song for kids",
                    thumbnailUrl:
                        "https://tse1.mm.bing.net/th?id=OVP.2NENjvogk0zUF3Gh4aNqIgHgFo&pid=Api",
                    datePublished: "2019-01-23T16:30:17.0000000",
                    publisher: [
                        {
                            name: "YouTube",
                        },
                    ],
                    isAccessibleForFree: true,
                    contentUrl: "https://www.youtube.com/watch?v=NXn5BFye360",
                    hostPageUrl: "https://www.youtube.com/watch?v=NXn5BFye360",
                    encodingFormat: "mp4",
                    hostPageDisplayUrl:
                        "https://www.youtube.com/watch?v=NXn5BFye360",
                    width: 1280,
                    height: 720,
                    duration: "PT2M7S",
                    embedHtml:
                        '<iframe width="1280" height="720" src="http://www.youtube.com/embed/NXn5BFye360?autoplay=1" frameborder="0" allowfullscreen></iframe>',
                    allowHttpsEmbed: true,
                    viewCount: 1138265,
                    thumbnail: {
                        width: 160,
                        height: 120,
                    },
                    allowMobileEmbed: true,
                    isSuperfresh: false,
                },
                {
                    webSearchUrl:
                        "https://www.bing.com/videos/search?q=bonjour&view=detail&mid=B138C0CC7716386B3E5EB138C0CC7716386B3E5E",
                    name:
                        "[Bonjour 2019] -Bingerville s'\u00e9clate avec Boukary",
                    thumbnailUrl:
                        "https://tse3.mm.bing.net/th?id=OVP.qacO-Kvd4es99TVqVy1GBQEsDh&pid=Api",
                    datePublished: "2019-02-07T16:18:44.0000000",
                    publisher: [
                        {
                            name: "YouTube",
                        },
                    ],
                    isAccessibleForFree: true,
                    contentUrl: "https://www.youtube.com/watch?v=D3p91GJuQkw",
                    hostPageUrl: "https://www.youtube.com/watch?v=D3p91GJuQkw",
                    encodingFormat: "mp4",
                    hostPageDisplayUrl:
                        "https://www.youtube.com/watch?v=D3p91GJuQkw",
                    width: 1280,
                    height: 720,
                    duration: "PT24M43S",
                    embedHtml:
                        '<iframe width="1280" height="720" src="http://www.youtube.com/embed/D3p91GJuQkw?autoplay=1" frameborder="0" allowfullscreen></iframe>',
                    allowHttpsEmbed: true,
                    viewCount: 1872589,
                    thumbnail: {
                        width: 160,
                        height: 120,
                    },
                    allowMobileEmbed: true,
                    isSuperfresh: false,
                },
                {
                    webSearchUrl:
                        "https://www.bing.com/videos/search?q=bonjour&view=detail&mid=88FE29416B8DA7E5B0C888FE29416B8DA7E5B0C8",
                    name:
                        "BONJOUR 2020 : Quand Agalawal donne les cons\u00e9quences de la gal\u00e8re",
                    thumbnailUrl:
                        "https://tse4.mm.bing.net/th?id=OVP.BG1yjgUSTpwk6vIt4jqRmAEsDh&pid=Api",
                    datePublished: "2020-01-14T08:00:00.0000000",
                    publisher: [
                        {
                            name: "YouTube",
                        },
                    ],
                    isAccessibleForFree: true,
                    contentUrl: "https://www.youtube.com/watch?v=bLHrSJ7DJGg",
                    hostPageUrl: "https://www.youtube.com/watch?v=bLHrSJ7DJGg",
                    encodingFormat: "mp4",
                    hostPageDisplayUrl:
                        "https://www.youtube.com/watch?v=bLHrSJ7DJGg",
                    width: 1280,
                    height: 720,
                    duration: "PT9M46S",
                    embedHtml:
                        '<iframe width="1280" height="720" src="http://www.youtube.com/embed/bLHrSJ7DJGg?autoplay=1" frameborder="0" allowfullscreen></iframe>',
                    allowHttpsEmbed: true,
                    viewCount: 1063017,
                    thumbnail: {
                        width: 160,
                        height: 120,
                    },
                    allowMobileEmbed: true,
                    isSuperfresh: false,
                },
                {
                    webSearchUrl:
                        "https://www.bing.com/videos/search?q=bonjour&view=detail&mid=05672D1BC0BED8D1084E05672D1BC0BED8D1084E",
                    name:
                        '[Bonjour 2019] - "Papitou" s\u00e9duit le public de Dimbokro',
                    thumbnailUrl:
                        "https://tse4.mm.bing.net/th?id=OVP.xsDbulI5NR-GiQUF-aXoEAEsDh&pid=Api",
                    datePublished: "2019-02-07T16:17:32.0000000",
                    publisher: [
                        {
                            name: "YouTube",
                        },
                    ],
                    isAccessibleForFree: true,
                    contentUrl: "https://www.youtube.com/watch?v=7DmBV8t6czA",
                    hostPageUrl: "https://www.youtube.com/watch?v=7DmBV8t6czA",
                    encodingFormat: "mp4",
                    hostPageDisplayUrl:
                        "https://www.youtube.com/watch?v=7DmBV8t6czA",
                    width: 1280,
                    height: 720,
                    duration: "PT14M40S",
                    embedHtml:
                        '<iframe width="1280" height="720" src="http://www.youtube.com/embed/7DmBV8t6czA?autoplay=1" frameborder="0" allowfullscreen></iframe>',
                    allowHttpsEmbed: true,
                    viewCount: 1340811,
                    thumbnail: {
                        width: 160,
                        height: 120,
                    },
                    allowMobileEmbed: true,
                    isSuperfresh: false,
                },
                {
                    webSearchUrl:
                        "https://www.bing.com/videos/search?q=bonjour&view=detail&mid=080BD2430E427BABC20D080BD2430E427BABC20D",
                    name:
                        "Bonjour Sourire - Film Complet en Fran\u00e7ais (Com\u00e9die) 1956 | Louis de Fun\u00e8s",
                    thumbnailUrl:
                        "https://tse3.mm.bing.net/th?id=OVP.78Gqfm7o-RR_4hzpGeHoTwEsDh&pid=Api",
                    datePublished: "2020-07-22T17:30:03.0000000",
                    publisher: [
                        {
                            name: "YouTube",
                        },
                    ],
                    isAccessibleForFree: true,
                    contentUrl: "https://www.youtube.com/watch?v=u9FgZKEwhZE",
                    hostPageUrl: "https://www.youtube.com/watch?v=u9FgZKEwhZE",
                    encodingFormat: "mp4",
                    hostPageDisplayUrl:
                        "https://www.youtube.com/watch?v=u9FgZKEwhZE",
                    width: 1280,
                    height: 720,
                    duration: "PT1H17M14S",
                    embedHtml:
                        '<iframe width="1280" height="720" src="http://www.youtube.com/embed/u9FgZKEwhZE?autoplay=1" frameborder="0" allowfullscreen></iframe>',
                    allowHttpsEmbed: true,
                    viewCount: 172880,
                    thumbnail: {
                        width: 160,
                        height: 120,
                    },
                    allowMobileEmbed: true,
                    isSuperfresh: false,
                },
                {
                    webSearchUrl:
                        "https://www.bing.com/videos/search?q=bonjour&view=detail&mid=062B1E439CACDCEDDFDE062B1E439CACDCEDDFDE",
                    name:
                        "Bonjour 2020 | Spectacle du groupe Les Zinzins de l'Art",
                    thumbnailUrl:
                        "https://tse3.mm.bing.net/th?id=OVP.zxAQmAUAXCH0fg2ZLPZWSgEsDh&pid=Api",
                    datePublished: "2020-01-07T09:19:07.0000000",
                    publisher: [
                        {
                            name: "YouTube",
                        },
                    ],
                    isAccessibleForFree: true,
                    contentUrl: "https://www.youtube.com/watch?v=UeHaalmxE-o",
                    hostPageUrl: "https://www.youtube.com/watch?v=UeHaalmxE-o",
                    encodingFormat: "mp4",
                    hostPageDisplayUrl:
                        "https://www.youtube.com/watch?v=UeHaalmxE-o",
                    width: 1280,
                    height: 720,
                    duration: "PT1M21S",
                    embedHtml:
                        '<iframe width="1280" height="720" src="http://www.youtube.com/embed/UeHaalmxE-o?autoplay=1" frameborder="0" allowfullscreen></iframe>',
                    allowHttpsEmbed: true,
                    viewCount: 234325,
                    thumbnail: {
                        width: 160,
                        height: 120,
                    },
                    allowMobileEmbed: true,
                    isSuperfresh: false,
                },
                {
                    webSearchUrl:
                        "https://www.bing.com/videos/search?q=bonjour&view=detail&mid=4960AB1EB3A1A37AB0824960AB1EB3A1A37AB082",
                    name:
                        'BONJOUR 2020 | "La f\u00eate de No\u00ebl " selon Bony la Merveille',
                    thumbnailUrl:
                        "https://tse4.mm.bing.net/th?id=OVP.3AqfHrAV6Sqf9QKNaYxkSAEsDh&pid=Api",
                    datePublished: "2020-01-06T08:00:00.0000000",
                    publisher: [
                        {
                            name: "YouTube",
                        },
                    ],
                    isAccessibleForFree: true,
                    contentUrl: "https://www.youtube.com/watch?v=VTv6WFxlNwI",
                    hostPageUrl: "https://www.youtube.com/watch?v=VTv6WFxlNwI",
                    encodingFormat: "mp4",
                    hostPageDisplayUrl:
                        "https://www.youtube.com/watch?v=VTv6WFxlNwI",
                    width: 1280,
                    height: 720,
                    duration: "PT1M22S",
                    embedHtml:
                        '<iframe width="1280" height="720" src="http://www.youtube.com/embed/VTv6WFxlNwI?autoplay=1" frameborder="0" allowfullscreen></iframe>',
                    allowHttpsEmbed: true,
                    viewCount: 304932,
                    thumbnail: {
                        width: 160,
                        height: 120,
                    },
                    allowMobileEmbed: true,
                    isSuperfresh: false,
                },
                {
                    webSearchUrl:
                        "https://www.bing.com/videos/search?q=bonjour&view=detail&mid=1AE2759EF0BF1673777C1AE2759EF0BF1673777C",
                    name:
                        "Bonjour Tristesse #93 : Praud, Macron Castaner un trio d'enfer",
                    thumbnailUrl:
                        "https://tse2.mm.bing.net/th?id=OVP.N7nnG-h8TNDxlPs4LtaOugEsDh&pid=Api",
                    datePublished: "2019-05-08T08:00:00.0000000",
                    publisher: [
                        {
                            name: "YouTube",
                        },
                    ],
                    isAccessibleForFree: true,
                    contentUrl: "https://www.youtube.com/watch?v=ubu-J3kdNGs",
                    hostPageUrl: "https://www.youtube.com/watch?v=ubu-J3kdNGs",
                    encodingFormat: "mp4",
                    hostPageDisplayUrl:
                        "https://www.youtube.com/watch?v=ubu-J3kdNGs",
                    width: 1280,
                    height: 720,
                    duration: "PT8M38S",
                    embedHtml:
                        '<iframe width="1280" height="720" src="http://www.youtube.com/embed/ubu-J3kdNGs?autoplay=1" frameborder="0" allowfullscreen></iframe>',
                    allowHttpsEmbed: true,
                    viewCount: 249060,
                    thumbnail: {
                        width: 160,
                        height: 120,
                    },
                    allowMobileEmbed: true,
                    isSuperfresh: false,
                },
            ],
            scenario: "List",
        },
        rankingResponse: {
            mainline: {
                items: [
                    {
                        answerType: "News",
                        value: {
                            id:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#News",
                        },
                    },
                    {
                        answerType: "WebPages",
                        resultIndex: 0,
                        value: {
                            id:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.0",
                        },
                    },
                    {
                        answerType: "WebPages",
                        resultIndex: 1,
                        value: {
                            id:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.1",
                        },
                    },
                    {
                        answerType: "Places",
                    },
                    {
                        answerType: "Places",
                        resultIndex: 0,
                        value: {
                            id:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#Places.0",
                        },
                    },
                    {
                        answerType: "Places",
                        resultIndex: 1,
                        value: {
                            id:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#Places.1",
                        },
                    },
                    {
                        answerType: "WebPages",
                        resultIndex: 2,
                        value: {
                            id:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.2",
                        },
                    },
                    {
                        answerType: "WebPages",
                        resultIndex: 3,
                        value: {
                            id:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.3",
                        },
                    },
                    {
                        answerType: "Videos",
                        value: {
                            id:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#Videos",
                        },
                    },
                    {
                        answerType: "WebPages",
                        resultIndex: 4,
                        value: {
                            id:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.4",
                        },
                    },
                    {
                        answerType: "WebPages",
                        resultIndex: 5,
                        value: {
                            id:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.5",
                        },
                    },
                    {
                        answerType: "WebPages",
                        resultIndex: 6,
                        value: {
                            id:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.6",
                        },
                    },
                ],
            },
            sidebar: {
                items: [
                    {
                        answerType: "Entities",
                        resultIndex: 0,
                        value: {
                            id:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#Entities.0",
                        },
                    },
                    {
                        answerType: "Places",
                        resultIndex: 2,
                        value: {
                            id:
                                "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#Places.2",
                        },
                    },
                ],
            },
        },
        images: {
            "id": "https:\/\/vr-search-engine.cognitiveservices.azure.com\/api\/v7\/#Images",
            "readLink": "https:\/\/vr-search-engine.cognitiveservices.azure.com\/api\/v7\/images\/search?q=coucou&qpvt=coucou",
            "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&qpvt=coucou",
            "isFamilyFriendly": true,
            "value": [
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=4BA84481EEA0FBD15F9E8783EC2AE6A9CDACA4C3&FORM=IQFRBA",
                    "name": "La taille des \u0153ufs de coucous, par Herv\u00e9 Lehning",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.BHgAOEKL1_8-hPdevEh7LQHaFj&pid=Api",
                    "datePublished": "2019-12-25T17:51:00.0000000Z",
                    "contentUrl": "https:\/\/blogs.futura-sciences.com\/lehning\/wp-content\/uploads\/sites\/13\/2018\/05\/coucou0.jpg",
                    "hostPageUrl": "https:\/\/blogs.futura-sciences.com\/lehning\/2018\/12\/02\/la-taille-des-oeufs-de-coucous\/",
                    "contentSize": "396481 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "https:\/\/blogs.futura-sciences.com\/lehning\/2018\/12\/02\/la-taille-des-oeufs-de-coucous\/",
                    "width": 1067,
                    "height": 800,
                    "thumbnail": {
                        "width": 474,
                        "height": 355
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=70F93967850EF9E5243C4AC95E6AAE4ADC5FCF82&FORM=IQFRBA",
                    "name": "Coucou gris m\u00e2le adulte - dede33633",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.DND6E8dzeMDdnKidizfWjgHaFj&pid=Api",
                    "datePublished": "2019-10-20T11:10:00.0000000Z",
                    "contentUrl": "http:\/\/www.oiseaux.net\/photos\/devashish.deb\/images\/coucou.gris.dede.1g.jpg",
                    "hostPageUrl": "http:\/\/devashish.deb.oiseaux.net\/coucou.gris.1.html",
                    "contentSize": "73518 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "http:\/\/devashish.deb.oiseaux.net\/coucou.gris.1.html",
                    "width": 800,
                    "height": 600,
                    "thumbnail": {
                        "width": 474,
                        "height": 355
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=14A1AEED9C00230705B83A5FB19C99C02207BBE4&FORM=IQFRBA",
                    "name": "Le coucou gris, un bel enfoir\u00e9 ! - Ici et l\u00e0 magazine",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.DzafPdcFvblTGe_B64rJJwHaE7&pid=Api",
                    "datePublished": "2020-07-14T12:33:00.0000000Z",
                    "contentUrl": "https:\/\/icietla-magazine.com\/wp-content\/uploads\/2018\/06\/coucou.jpg",
                    "hostPageUrl": "https:\/\/icietla-magazine.com\/coucou-gris-bel-enfoire\/",
                    "contentSize": "264593 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "https:\/\/icietla-magazine.com\/coucou-gris-bel-enfoire\/",
                    "width": 1000,
                    "height": 665,
                    "thumbnail": {
                        "width": 474,
                        "height": 315
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=377490A4BFF2512478AE9C877668DA2E14BB913F&FORM=IQFRBA",
                    "name": "Le coucou sans scrupule ! - BIENVENUE CHEZ SYLVIE DE BATZ",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.FeFIdxWnNP0OTOWrjmmpFQHaFo&pid=Api",
                    "datePublished": "2020-12-12T07:18:00.0000000Z",
                    "contentUrl": "http:\/\/haute-normandie.lpo.fr\/atlas-regional\/coucou_gris\/ill_max.jpg",
                    "hostPageUrl": "http:\/\/conseils-astuces.over-blog.com\/article-le-72776823.html",
                    "contentSize": "249956 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "http:\/\/conseils-astuces.over-blog.com\/article-le-72776823.html",
                    "width": 700,
                    "height": 533,
                    "thumbnail": {
                        "width": 474,
                        "height": 360
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=90E281DF306BFF6BB1936EA2A045C83EE238816B&FORM=IQFRBA",
                    "name": "Coucou gris adulte - reho22992",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.eMpNLgt72KVnAdWzr28P_wHaFm&pid=Api",
                    "datePublished": "2019-12-18T17:53:00.0000000Z",
                    "contentUrl": "https:\/\/www.oiseaux.net\/photos\/rein.hofman\/images\/coucou.gris.reho.1g.jpg",
                    "hostPageUrl": "https:\/\/www.oiseaux.net\/photos\/rein.hofman\/coucou.gris.1.html",
                    "contentSize": "69464 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "https:\/\/www.oiseaux.net\/photos\/rein.hofman\/coucou.gris.1.html",
                    "width": 800,
                    "height": 605,
                    "thumbnail": {
                        "width": 474,
                        "height": 358
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=CAB0563C1D1F50FE785B6AC752CDC56251E5EDAA&FORM=IQFRBA",
                    "name": "Coucou gris juv\u00e9nile - reho22994",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.qc-23xIsL4_tmtBRoIIOIQHaFF&pid=Api",
                    "datePublished": "2010-02-09T19:35:00.0000000Z",
                    "contentUrl": "http:\/\/www.oiseaux.net\/photos\/rein.hofman\/images\/coucou.gris.reho.3g.jpg",
                    "hostPageUrl": "http:\/\/www.oiseaux.net\/photos\/rein.hofman\/coucou.gris.3.html",
                    "contentSize": "129696 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "http:\/\/www.oiseaux.net\/photos\/rein.hofman\/coucou.gris.3.html",
                    "width": 800,
                    "height": 550,
                    "thumbnail": {
                        "width": 474,
                        "height": 325
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=809F4DD092F2FC2F1CA9E0AE7ED3EB3A2D63D1D9&FORM=IQFRBA",
                    "name": "Le coucou gris (Cuculus canorus)",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.EElxoKEVtGM_Cxb3CmPZEQHaE3&pid=Api",
                    "datePublished": "2020-01-26T19:01:00.0000000Z",
                    "contentUrl": "http:\/\/viagallica.com\/a\/img\/coucou_gris_001.jpg",
                    "hostPageUrl": "http:\/\/viagallica.com\/a\/coucou_gris.htm",
                    "contentSize": "43630 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "http:\/\/viagallica.com\/a\/coucou_gris.htm",
                    "width": 600,
                    "height": 394,
                    "thumbnail": {
                        "width": 474,
                        "height": 311
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=23268A1F343C51271F32A4C2A2586EF42FE27BE9&FORM=IQFRBA",
                    "name": "Une \u00e9tude met en lumi\u00e8re l'\u00e9trange comportement de la ...",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.17FUYc3TlOGkwQky-laDpgHaE7&pid=Api",
                    "datePublished": "2020-01-06T11:32:00.0000000Z",
                    "contentUrl": "https:\/\/fr.metrotime.be\/wp-content\/uploads\/2017\/09\/Common_Cuckoo_Cuculus_canorus_8079424957.jpg",
                    "hostPageUrl": "https:\/\/fr.metrotime.be\/2017\/09\/10\/must-read\/etude-met-lumiere-letrange-comportement-de-femelle-coucou\/",
                    "contentSize": "28850 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "https:\/\/fr.metrotime.be\/2017\/09\/10\/must-read\/etude-met-lumiere-letrange-comportement-de-femelle-coucou\/",
                    "width": 800,
                    "height": 533,
                    "thumbnail": {
                        "width": 474,
                        "height": 315
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=F2F03A3126D5EA89CF8854F98217A1E04690D9B4&FORM=IQFRBA",
                    "name": "Coucou gris : Fiche descriptive de l'oiseau + photos ...",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.bsj18g74THeDfpxyjdbq0gHaE8&pid=Api",
                    "datePublished": "2020-10-08T20:31:00.0000000Z",
                    "contentUrl": "https:\/\/www.instinct-animal.fr\/wp-content\/uploads\/2019\/05\/coucou-gris-photo.jpg",
                    "hostPageUrl": "https:\/\/www.instinct-animal.fr\/oiseaux\/coucou-gris\/",
                    "contentSize": "22265 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "https:\/\/www.instinct-animal.fr\/oiseaux\/coucou-gris\/",
                    "width": 640,
                    "height": 427,
                    "thumbnail": {
                        "width": 474,
                        "height": 316
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=7628C9A5E7D2DF6CA66EDDCAFFD4E6F45CDB7F65&FORM=IQFRBA",
                    "name": "Coucou geai immature - thhe36197",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.W28YKshc-6gu78vUKxJ9gwHaEu&pid=Api",
                    "datePublished": "2019-11-12T06:04:00.0000000Z",
                    "contentUrl": "https:\/\/www.oiseaux.net\/photos\/thierry.helsens\/images\/coucou.geai.thhe.1g.jpg",
                    "hostPageUrl": "https:\/\/www.oiseaux.net\/photos\/thierry.helsens\/coucou.geai.1.html",
                    "contentSize": "138713 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "https:\/\/www.oiseaux.net\/photos\/thierry.helsens\/coucou.geai.1.html",
                    "width": 800,
                    "height": 511,
                    "thumbnail": {
                        "width": 474,
                        "height": 302
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=11478CC0784BBCA63C34553A7F50121DE37CA4F5&FORM=IQFRBA",
                    "name": "Sabelpoot coucou, coqs et poules de race : la Ferme de ...",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.Q_J2h_xxJMKPNoNjSc3NoAHaF7&pid=Api",
                    "datePublished": "2019-11-08T19:16:00.0000000Z",
                    "contentUrl": "https:\/\/static.fermedebeaumont.com\/images\/poules-coqs-race\/naines\/sabelpoot-coucou-coq-poule-race-elevage.jpg",
                    "hostPageUrl": "https:\/\/www.fermedebeaumont.com\/sabelpoot-coucou-p-10698.html",
                    "contentSize": "207433 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "https:\/\/www.fermedebeaumont.com\/sabelpoot-coucou-p-10698.html",
                    "width": 920,
                    "height": 736,
                    "thumbnail": {
                        "width": 474,
                        "height": 379
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=E4C457B20089FE69FF0D8C7FB4C92ACDDA214141&FORM=IQFRBA",
                    "name": "Coucou t'es o\u00f9, le n\u00b0251 de la revue La Salamandre est sorti",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.arNXXKicQ6CxQdPZcFsiawHaEX&pid=Api",
                    "datePublished": "2019-09-16T01:19:00.0000000Z",
                    "contentUrl": "https:\/\/master.salamandre.net\/media\/21903\/Salamandre251Coucou-1800x1062.jpg",
                    "hostPageUrl": "https:\/\/www.salamandre.org\/article\/coucou-tes-ou-salamandre-n251\/",
                    "contentSize": "295677 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "https:\/\/www.salamandre.org\/article\/coucou-tes-ou-salamandre-n251\/",
                    "width": 1800,
                    "height": 1062,
                    "thumbnail": {
                        "width": 474,
                        "height": 279
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=BEDB744C12C0F23CC967E6CB145E9D2C0D98C950&FORM=IQFRBA",
                    "name": "Coucou, oiseau tueur ! - blog-des-decouvertes.com",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.v45GpHg7Pai_v7LOc_D-gwHaE9&pid=Api",
                    "datePublished": "2014-06-13T19:08:00.0000000Z",
                    "contentUrl": "http:\/\/img.over-blog-kiwi.com\/0\/81\/89\/33\/201311\/ob_48982c18cff918f9557ff76c42a433ba_coucou-gris-jlco-6g.jpg",
                    "hostPageUrl": "http:\/\/equi-and-co.over-blog.com\/2013\/11\/coucou-oiseau-tueur.html",
                    "contentSize": "58713 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "http:\/\/equi-and-co.over-blog.com\/2013\/11\/coucou-oiseau-tueur.html",
                    "width": 800,
                    "height": 536,
                    "thumbnail": {
                        "width": 474,
                        "height": 317
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=1F63F7FED832B73FDA2B3554EF4D880CBBF4FD61&FORM=IQFRBA",
                    "name": "Coucou gris 1\u00e8re ann\u00e9e - auau55901",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.OgUPyDwSm-rHEexxQPswUAHaFs&pid=Api",
                    "datePublished": "2019-11-05T19:05:00.0000000Z",
                    "contentUrl": "https:\/\/www.oiseaux.net\/photos\/aurelien.audevard\/images\/coucou.gris.auau.8g.jpg",
                    "hostPageUrl": "https:\/\/www.oiseaux.net\/photos\/aurelien.audevard\/coucou.gris.8.html",
                    "contentSize": "76387 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "https:\/\/www.oiseaux.net\/photos\/aurelien.audevard\/coucou.gris.8.html",
                    "width": 800,
                    "height": 616,
                    "thumbnail": {
                        "width": 474,
                        "height": 364
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=98F44BB1B898AB3E5A4C075199B8E0390BBE3878&FORM=IQFRBA",
                    "name": "Coucou gris - Cuculus canorus",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.QaCmpPWjpAsTkJNbcBtMYwAAAA&pid=Api",
                    "datePublished": "2019-09-19T21:34:00.0000000Z",
                    "contentUrl": "http:\/\/www.oiseaux.net\/photos\/gerard.vigo\/images\/id\/coucou.gris.gevi.2p.jpg",
                    "hostPageUrl": "http:\/\/www.oiseaux.net\/oiseaux\/coucou.gris.html",
                    "contentSize": "28206 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "http:\/\/www.oiseaux.net\/oiseaux\/coucou.gris.html",
                    "width": 300,
                    "height": 300,
                    "thumbnail": {
                        "width": 300,
                        "height": 300
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=E7B488669E13A9CD16B5D5F1A125350667A65FE5&FORM=IQFRBA",
                    "name": "Globetrotters - Migration : 1 ann\u00e9e, 12 esp\u00e8ces embl\u00e9matiques",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.5vGouvdR1XFdau_NhZ0vyQHaG2&pid=Api",
                    "datePublished": "2020-01-15T09:56:00.0000000Z",
                    "contentUrl": "http:\/\/www.alienor.org\/publications\/oiseau-migration\/images\/large\/edu02_hn998-1-109_coucou_56589.jpg",
                    "hostPageUrl": "http:\/\/www.alienor.org\/publications\/oiseau-migration\/calendrier2.php",
                    "contentSize": "143329 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "http:\/\/www.alienor.org\/publications\/oiseau-migration\/calendrier2.php",
                    "width": 1024,
                    "height": 947,
                    "thumbnail": {
                        "width": 474,
                        "height": 438
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=98F44BB1B898AB3E5A4C2A0A2E662FF83F65B63B&FORM=IQFRBA",
                    "name": "Coucou gris - Cuculus canorus",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.czwljRngXG7JJHvbUa38-QHaFj&pid=Api",
                    "datePublished": "2017-02-16T14:14:00.0000000Z",
                    "contentUrl": "http:\/\/rene.dumoulin.oiseaux.net\/images\/coucou.gris.redu.8g.jpg",
                    "hostPageUrl": "http:\/\/www.oiseaux.net\/oiseaux\/coucou.gris.html",
                    "contentSize": "49393 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "http:\/\/www.oiseaux.net\/oiseaux\/coucou.gris.html",
                    "width": 800,
                    "height": 600,
                    "thumbnail": {
                        "width": 474,
                        "height": 355
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=880835BEA8B198F72F557BBE632F832F443BE4FC&FORM=IQFRBA",
                    "name": "Coucou gris - jogo37590",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.ZzJnr7LHrbL91A7dFUfwlwHaJk&pid=Api",
                    "datePublished": "2010-02-20T17:06:00.0000000Z",
                    "contentUrl": "http:\/\/www.oiseaux.net\/photos\/john.gould\/images\/coucou.gris.jogo.0g.jpg",
                    "hostPageUrl": "http:\/\/www.oiseaux.net\/photos\/john.gould\/coucou.gris.0.html",
                    "contentSize": "92086 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "http:\/\/www.oiseaux.net\/photos\/john.gould\/coucou.gris.0.html",
                    "width": 618,
                    "height": 799,
                    "thumbnail": {
                        "width": 474,
                        "height": 612
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=85D1D38A167665ADA0F3D89D7EBF218390F2DC91&FORM=IQFRBA",
                    "name": "Le coucou gris \u2013 En for\u00eat avec Manon",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.VcX22-b0D74S5E6HrKogzQHaHa&pid=Api",
                    "datePublished": "2019-03-19T02:46:00.0000000Z",
                    "contentUrl": "https:\/\/enforetavecmanon.files.wordpress.com\/2017\/06\/coucou.jpg?w=1000",
                    "hostPageUrl": "https:\/\/enforetavecmanon.wordpress.com\/2017\/06\/26\/le-coucou-gris\/",
                    "contentSize": "60614 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "https:\/\/enforetavecmanon.wordpress.com\/2017\/06\/26\/le-coucou-gris\/",
                    "width": 800,
                    "height": 800,
                    "thumbnail": {
                        "width": 474,
                        "height": 474
                    }
                },
                {
                    "webSearchUrl": "https:\/\/www.bing.com\/images\/search?q=coucou&id=096C2463986C5E43D14265DC027CE6324678E982&FORM=IQFRBA",
                    "name": "Coucou de Rennes \u2014 Wikip\u00e9dia",
                    "thumbnailUrl": "https:\/\/tse1.mm.bing.net\/th?id=OIP.XriYaOAByKeGytb2jMAyggHaE8&pid=Api",
                    "datePublished": "2020-01-04T18:15:00.0000000Z",
                    "contentUrl": "https:\/\/upload.wikimedia.org\/wikipedia\/commons\/thumb\/4\/47\/Coq_coucou_de_Rennes-2.jpg\/1200px-Coq_coucou_de_Rennes-2.jpg",
                    "hostPageUrl": "https:\/\/fr.wikipedia.org\/wiki\/Coucou_de_Rennes",
                    "contentSize": "304021 B",
                    "encodingFormat": "jpeg",
                    "hostPageDisplayUrl": "https:\/\/fr.wikipedia.org\/wiki\/Coucou_de_Rennes",
                    "width": 1200,
                    "height": 800,
                    "thumbnail": {
                        "width": 474,
                        "height": 316
                    }
                }
            ]
        },
        relatedSearches: {
            "id": "https:\/\/vr-search-engine.cognitiveservices.azure.com\/api\/v7\/#RelatedSearches",
            "value": [
                {
                    "text": "faire coucou definition",
                    "displayText": "faire coucou definition",
                    "webSearchUrl": "https:\/\/www.bing.com\/search?q=faire+coucou+d%C3%A9finition"
                },
                {
                    "text": "coucou signification",
                    "displayText": "coucou signification",
                    "webSearchUrl": "https:\/\/www.bing.com\/search?q=coucou+signification"
                },
                {
                    "text": "coucou definition",
                    "displayText": "coucou definition",
                    "webSearchUrl": "https:\/\/www.bing.com\/search?q=coucou+d%C3%A9finition"
                },
                {
                    "text": "coucou en anglais",
                    "displayText": "coucou en anglais",
                    "webSearchUrl": "https:\/\/www.bing.com\/search?q=coucou+en+anglais"
                },
                {
                    "text": "coucou synonyme",
                    "displayText": "coucou synonyme",
                    "webSearchUrl": "https:\/\/www.bing.com\/search?q=coucou+synonyme"
                },
                {
                    "text": "coucou oiseau",
                    "displayText": "coucou oiseau",
                    "webSearchUrl": "https:\/\/www.bing.com\/search?q=coucou+oiseau"
                },
                {
                    "text": "horloge coucou",
                    "displayText": "horloge coucou",
                    "webSearchUrl": "https:\/\/www.bing.com\/search?q=horloge+coucou"
                },
                {
                    "text": "cuculus canorus",
                    "displayText": "cuculus canorus",
                    "webSearchUrl": "https:\/\/www.bing.com\/search?q=cuculus+canorus"
                }
            ]
        }
    },
];

const fakeresultwebxr = [
    {
        "BingAPIs-TraceId": "1D5E482675554BFA853888F9B905B4B7",
        "BingAPIs-SessionId": "F728CDE1E7024CBBA3FDE991A2E38261",
        "X-MSEdge-ClientID": "23A70735B2386ABF3A9A08D6B3E76BFD",
        "BingAPIs-Market": "fr-FR",
        "X-MSEdge-Ref": "Ref A: 1D5E482675554BFA853888F9B905B4B7 Ref B: HEL01EDGE0721 Ref C: 2021-02-19T08:06:20Z"
    },
    {
        "_type": "SearchResponse",
        "queryContext": {
            "originalQuery": "webxr"
        },
        "webPages": {
            "webSearchUrl": "https://www.bing.com/search?q=webxr",
            "totalEstimatedMatches": 179000,
            "value": [
                {
                    "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.0",
                    "name": "Hello WebXR! - Mozilla Mixed Reality",
                    "url": "https://mixedreality.mozilla.org/hello-webxr/",
                    "thumbnailUrl": "https://www.bing.com/th?id=OIP.z3sAU1M1LP10MUVIhMVY0wHaEI&pid=Api",
                    "isFamilyFriendly": true,
                    "displayUrl": "https://mixedreality.mozilla.org/hello-webxr",
                    "snippet": "WebXR multi-experience by the Mozilla Mixed Reality team to celebrate the official release of the WebXR specification.",
                    "dateLastCrawled": "2021-02-16T21:27:00.0000000Z",
                    "language": "en",
                    "isNavigational": false
                },
                {
                    "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.1",
                    "name": "Utilisation de WebXR avec Windows Mixed Reality - Mixed ...",
                    "url": "https://docs.microsoft.com/fr-fr/windows/mixed-reality/develop/web/webxr-overview",
                    "isFamilyFriendly": true,
                    "displayUrl": "https://docs.microsoft.com/fr-fr/windows/mixed-reality/develop/web/webxr-overview",
                    "snippet": "Présentation de WebXR WebXR Overview. 04/10/2020; 2 minutes de lecture; Y; o; Dans cet article Qu’est-ce que WebXR What is WebXR. L' API d’appareil WebXR permet d’accéder à des appareils de réalité virtuelle (VR) et de réalité augmentée (AR), notamment des capteurs et des affichages montés en tête sur le Web. The WebXR device API is for accessing virtual reality (VR) and ...",
                    "dateLastCrawled": "2021-01-31T07:00:00.0000000Z",
                    "searchTags": [
                        {
                            "name": "search.ms_docsetname",
                            "content": "\"mixed-reality-docs\"; mixed; reality; docs"
                        },
                        {
                            "name": "search.ms_product",
                            "content": "\"MSDN\"; msdn"
                        },
                        {
                            "name": "search.ms_sitename",
                            "content": "\"Docs\"; docs"
                        }
                    ],
                    "language": "fr",
                    "isNavigational": false
                },
                {
                    "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.2",
                    "name": "L'API de périphérique WebXR - Référence Web API | MDN",
                    "url": "https://developer.mozilla.org/fr/docs/Web/API/WebXR_Device_API",
                    "isFamilyFriendly": true,
                    "displayUrl": "https://developer.mozilla.org/fr/docs/Web/API/WebXR_Device_API",
                    "snippet": "WebXR est un ensemble de standards utilisés pour supporter le rendu de scènes 3D vers du matériel conçu pour présenter des mondes virtuels (Réalité Virtuelle, ou VR), ou pour ajouter des contenus graphiques dans le monde réel, (Réalité Augmentée, ou AR).",
                    "dateLastCrawled": "2020-12-12T08:15:00.0000000Z",
                    "language": "fr",
                    "isNavigational": false
                },
                {
                    "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.3",
                    "name": "Using WebXR with Windows Mixed Reality - Mixed Reality ...",
                    "url": "https://docs.microsoft.com/en-us/windows/mixed-reality/develop/web/webxr-overview",
                    "isFamilyFriendly": true,
                    "displayUrl": "https://docs.microsoft.com/en-us/windows/mixed-reality/develop/web/webxr-overview",
                    "snippet": "What is WebXR. The WebXR device API is for accessing virtual reality (VR) and augmented reality (AR) devices, including sensors and head-mounted displays on the Web. WebXR device API is currently available on Microsoft Edge and Chrome version 79 and later versions supports WebXR as a default.",
                    "dateLastCrawled": "2021-02-16T21:06:00.0000000Z",
                    "searchTags": [
                        {
                            "name": "search.ms_docsetname",
                            "content": "\"mixed-reality-docs\"; mixed; reality; docs"
                        },
                        {
                            "name": "search.ms_product",
                            "content": "\"MSDN\"; msdn"
                        },
                        {
                            "name": "search.ms_sitename",
                            "content": "\"Docs\"; docs"
                        }
                    ],
                    "language": "en",
                    "isNavigational": false
                },
                {
                    "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.4",
                    "name": "WebXR Games - Play VR & AR online! Play virtual reality ...",
                    "url": "https://webxr.games/",
                    "isFamilyFriendly": true,
                    "displayUrl": "https://webxr.games",
                    "snippet": "Play WebXR games - virtual reality (VR) and augmented reality (AR) devices, including vr headsets, sensors and head-mounted displays, on the Web. Games. What is WebXR? XR is an acronym short for “Extended Reality”, which includes augmented reality, virtual reality, mixed reality, AR, VR, MR respectively, and all other real-and-virtual combined ...",
                    "dateLastCrawled": "2021-02-16T04:29:00.0000000Z",
                    "language": "en",
                    "isNavigational": false
                },
                {
                    "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.5",
                    "name": "Informations sur la permission WebXR | Assistance de Firefox",
                    "url": "https://support.mozilla.org/fr/kb/informations-permission-webxr",
                    "isFamilyFriendly": true,
                    "displayUrl": "https://support.mozilla.org/fr/kb/informations-permission-webxr",
                    "snippet": "Informations sur la permission WebXR Veuillez mettre à jour votre version de Firefox pour profiter des dernières fonctionnalités et mises à jour de sécurité. Cet article détaille les fonctionnalités de réalité virtuelle de Firefox et explique comment gérer les permissions des sites web qui demandent l’accès à vos appareils de réalité virtuelle.",
                    "dateLastCrawled": "2021-02-06T17:04:00.0000000Z",
                    "language": "fr",
                    "isNavigational": false
                },
                {
                    "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.6",
                    "name": "WebXR - Samples - GitHub Pages",
                    "url": "https://immersive-web.github.io/webxr-samples/",
                    "isFamilyFriendly": true,
                    "displayUrl": "https://immersive-web.github.io/webxr-samples",
                    "snippet": "Sample pages demonstrating how to use various aspects of the WebXR API. Learn More. Models used in these samples come from Poly, and many were modeled in Blocks. They a stored and loaded using the glTF 2.0 format. Attribution for individual models can be found under the media/gltf folders for this repository.. View samples source on GitHub",
                    "dateLastCrawled": "2021-02-16T08:04:00.0000000Z",
                    "language": "en",
                    "isNavigational": false
                },
                {
                    "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.7",
                    "name": "WebXR Device API - GitHub Pages",
                    "url": "https://immersive-web.github.io/webxr/",
                    "isFamilyFriendly": true,
                    "displayUrl": "https://immersive-web.github.io/webxr",
                    "snippet": "WebXR provides various transforms in the form of matrices. WebXR uses the WebGL conventions when communicating matrices, in which 4x4 matrices are given as 16 element Float32Array s with column major storage, and are applied to column vectors by premultiplying the matrix from the left.",
                    "dateLastCrawled": "2021-02-15T07:00:00.0000000Z",
                    "language": "en",
                    "isNavigational": false
                },
                {
                    "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.8",
                    "name": "webxr.fr - V4d.fr",
                    "url": "https://www.webxr.fr/",
                    "isFamilyFriendly": true,
                    "displayUrl": "https://www.webxr.fr",
                    "snippet": "Connexion à V4d. GO",
                    "dateLastCrawled": "2021-02-13T18:46:00.0000000Z",
                    "language": "fr",
                    "isNavigational": false
                },
                {
                    "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.9",
                    "name": "WebXR.io - Home",
                    "url": "https://webxr.io/",
                    "isFamilyFriendly": true,
                    "displayUrl": "https://webxr.io",
                    "snippet": "WebXR.ioio WebAR Playground Augmented Website Newsletter AR-Code Generator ARKit App Marker Generator Augmented Website Newsletter AR-Code Generator ARKit App Marker Generator",
                    "dateLastCrawled": "2021-02-16T04:29:00.0000000Z",
                    "language": "en",
                    "isNavigational": false
                }
            ]
        },
        "relatedSearches": {
            "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#RelatedSearches",
            "value": [
                {
                    "text": "webxr edge",
                    "displayText": "webxr edge",
                    "webSearchUrl": "https://www.bing.com/search?q=webxr+edge"
                },
                {
                    "text": "webxr samples",
                    "displayText": "webxr samples",
                    "webSearchUrl": "https://www.bing.com/search?q=webxr+samples"
                }
            ]
        },
        "rankingResponse": {
            "mainline": {
                "items": [
                    {
                        "answerType": "WebPages",
                        "resultIndex": 0,
                        "value": {
                            "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.0"
                        }
                    },
                    {
                        "answerType": "WebPages",
                        "resultIndex": 1,
                        "value": {
                            "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.1"
                        }
                    },
                    {
                        "answerType": "WebPages",
                        "resultIndex": 2,
                        "value": {
                            "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.2"
                        }
                    },
                    {
                        "answerType": "WebPages",
                        "resultIndex": 3,
                        "value": {
                            "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.3"
                        }
                    },
                    {
                        "answerType": "WebPages",
                        "resultIndex": 4,
                        "value": {
                            "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.4"
                        }
                    },
                    {
                        "answerType": "WebPages",
                        "resultIndex": 5,
                        "value": {
                            "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.5"
                        }
                    },
                    {
                        "answerType": "WebPages",
                        "resultIndex": 6,
                        "value": {
                            "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.6"
                        }
                    },
                    {
                        "answerType": "WebPages",
                        "resultIndex": 7,
                        "value": {
                            "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.7"
                        }
                    },
                    {
                        "answerType": "WebPages",
                        "resultIndex": 8,
                        "value": {
                            "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.8"
                        }
                    },
                    {
                        "answerType": "WebPages",
                        "resultIndex": 9,
                        "value": {
                            "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#WebPages.9"
                        }
                    }
                ]
            },
            "sidebar": {
                "items": [
                    {
                        "answerType": "RelatedSearches",
                        "value": {
                            "id": "https://vr-search-engine.cognitiveservices.azure.com/api/v7/#RelatedSearches"
                        }
                    }
                ]
            }
        }
    }
]

var i = 1;
function fkrst() {
    let results = [fakeresultwebxr, fakeresultMixed];
    let retour = results[i++ % results.length];
    //console.log(retour);
    return retour;
}




AFRAME.registerComponent("search-manager", {
    schema: {
        keyboard: { type: "selector" }, // super keyboard selector
        page: { type: "number" }, // page number
        results: { type: "number" }, //
        triggerDelay: { type: "number", default: 50 },
        maxCaractere: { type: "number", default: 10 },
        debug: { type: "boolean", default: false },
        hideOnResult: { type: "boolean", default: true },
    },

    //#region standard component

    init: function () {
        let data = this.data;
        let el = this.el;
        this.bindMethods();
        data.api = "https://xrsear.ch/api.php";
        // Do something when component first attached
        let presearchParam = new URLSearchParams(window.location.search).get(
            "q"
        );
        if (presearchParam) {
            this.sendrequest(presearchParam);
        }
        var pattern = /127.0.0.1/
        if(pattern.test( window.location.href)){
           this.data.debug = true;
        }
    },

    update: function (oldData) {
        //console.log("update");
        let data = this.data;

        // Make sure the page number exist
        if (data.page < 1) data.page = 1;

        if (data.keyboard == null) data.keyboard = oldData.keyboard;

        if (!data.keyboard.isSameNode(oldData.keyboard)) {
            //console.log("event setup");
            //if the keyboard have changed
            data.keyboard.addEventListener(
                "superkeyboardchanged",
                this.superkeyboardchanged
            );
            data.keyboard.addEventListener(
                "keyboard-dismissed",
                this.superkeyboarddismissed
            );
            data.keyboard.addEventListener(
                "keyboard-accepted",
                this.superkeyboardAccepted
            );

            if (oldData.keyboard) {
                //If there were a previous keyboard
                oldData.keyboard.removeEventListener(
                    "superkeyboardchanged",
                    this.superkeyboardchanged
                );
                oldData.keyboard.removeEventListener(
                    "keyboard-dismissed",
                    this.superkeyboarddismissed
                );
                oldData.keyboard.removeEventListener(
                    "keyboard-accepted",
                    this.superkeyboardAccepted
                );
            }


        }
    },

    //#endregion

    //#region XHR Function
    sendrequest: function (query) {
        let d = this.data;

        d.keyboard.setAttribute("super-keyboard", "show", !d.hideOnResult);

        //preparing url
        let url =
            "" +
            d.api +
            "?q=" +
            encodeURIComponent(_.trim(query)) +
            "&format=json&pageno=" +
            d.page;
        // console.log("url : " + url);

        //init XHR
        this.requestEl = new XMLHttpRequest();
        this.requestEl.open("GET", url);

        //Event Handling
        this.requestEl.addEventListener("progress", this.requestProgress);
        this.requestEl.addEventListener("load", this.requestFinished);
        this.requestEl.addEventListener("error", this.requestFailed);
        this.requestEl.addEventListener("abort", this.requestAborded);

        this.requestEl.send();
    },

    requestProgress: function (event) {
        console.log(event, this.requestEl);
        //TODO : create loading UI
    },

    requestFinished: function (event) {
        // console.log(event, this.requestEl);
        //console.log(this.requestEl.response);
        //TODO : manage response
        this.renderData(JSON.parse(this.requestEl.response));
        this.requestEl = null;
    },

    requestFailed: function (event) {
        //console.log(event, this.requestEl);
        //TODO : Error Handling
        if (this.data.debug) {
            //console.log(fakeresult);
            this.renderData(fkrst());
        } else {
            AFRAME.scenes[0].emit('showMessage', {
                title: "Failed request to server",
                content: "please retry in a few second. If the problem persist, please contact us. Thank you for your time!",
                symbole: "#warning"
            })
        }
        this.requestEl = null;
    },

    requestAborded: function (event) {
        console.log(event, this.requestEl);
        //TODO : Error Handling
        AFRAME.scenes[0].emit('showMessage', {
            title: "Aborted access to server",
            content: "please retry in a few second. If the problem persist, please contact us. Thank you for your time!",
            symbole: "#warning"
        })
        this.requestEl = null;
    },

    //#endregion

    //#region keyboard events
    superkeyboardchanged: function (event) {
        console.log(event);
    },

    superkeyboarddismissed: function (event) {
        console.log(event);
    },

    superkeyboardAccepted: function (event) {
        if (this.requestEl) return; //si une requette est déjà en cours
        console.log(event);
        this.sendrequest(event.detail.value);
        //TODO : Handle Multiple Requests
    },

    //#endregion

    // Result rendering

    renderData: function (qdata) {

        this.data.result = qdata;
        //console.log(qdata);
        let userSession = qdata[0];
        let result = qdata[1];
        let resultOrder = result.rankingResponse.mainline.items;
        let data = [];
        //Sorted data
        let newsList = result.news || {};
        newsList.name = "news";
        let newsListEntries = newsList.value = this.sanitize(newsList.value) || [];

        let videosList = result.videos || {};
        videosList.name = "videos";
        let videosListEntries = videosList.value = this.sanitize(videosList.value) || [];
        //debugger;

        let placesList = result.places || {};
        placesList.name = "places";
        let placesListEntries = placesList.value = this.sanitize(placesList.value) || [];

        let imagesList = result.images || {};
        imagesList.name = "images";
        let imagesListEntries = imagesList.value = this.sanitize(imagesList.value) || [];

        let webPagesList = result.webPages || {};
        webPagesList.name = "web";
        let webPagesListEntries = webPagesList.value = this.sanitize(webPagesList.value) || [];

        let relatedSearchesList = result.relatedSearches || {};
        relatedSearchesList.name = "related";
        let relatedSearchesListEntries = relatedSearchesList.value = this.sanitize(relatedSearchesList.value) || [];


        //if(newsListEntries.length != 0) data.push(newsList);
        if (videosListEntries.length != 0) data.push(videosList);
        if (webPagesListEntries.length != 0) data.push(webPagesList);
        if (imagesListEntries.length != 0) data.push(imagesList);

        //if(newsListEntries.length != 0) data.push(placesList);
        //if(relatedSearchesListEntries.length != 0) data.push(relatedSearchesList);


        //For each panel, we check the data resultType
        //console.log(resultPanels);
        //let data = [videosList,webPagesList,imagesList];
        /*
            resultPanels.forEach(resultPanel => {
                switch (resultPanel.components["result-panel-template"].data.resultType) {
                    case "media":
                        resultPanel.setAttribute("result-panel-template", {
                            resultData: JSON.stringify(_.take(videosListEntries, 9))
                        })
                        break;

                    default://page
                        resultPanel.setAttribute("result-panel-template", {
                            resultData: JSON.stringify(_.take(webPagesListEntries, 9))
                        })
                        break;
                }
            });
        */
        // console.log('emitting data');
        //debugger;
        AFRAME.scenes[0].emit('receiveQuery', data);
    },

    sanitize(list) {
        const sanitizablePath = ["name", "snippet"];
        //if it's an array  
        if (_.isArray(list)) {

            //foreach element in the array
            for (let i = 0; i < list.length; i++) {
                var el = list[i];
                //console.log(el);

                //we analyze each sanitizablePath
                sanitizablePath.forEach(path => {

                    let content = _.get(el, path);
                    //console.log(content);

                    //if the path exist
                    if (content !== undefined) {

                        //then we sanitize the string
                        _.set(el, path, _.replace(content, /;/g, "."));
                        //console.log(_.get(el, path));

                    }
                });



                //We apply the modification for safety reason
                list[i] = el;
            }
        }
        return list;
    },

    // miscelianous

    bindMethods: function () {
        //Data Binding
        this.sendrequest = this.sendrequest.bind(this);
        this.requestProgress = this.requestProgress.bind(this);
        this.requestFinished = this.requestFinished.bind(this);
        this.requestFailed = this.requestFailed.bind(this);
        this.requestAborded = this.requestAborded.bind(this);
        this.superkeyboardchanged = this.superkeyboardchanged.bind(this);
        this.superkeyboarddismissed = this.superkeyboarddismissed.bind(this);
        this.superkeyboardAccepted = this.superkeyboardAccepted.bind(this);
        this.renderData = this.renderData.bind(this);
    },
});

AFRAME.registerState({
    initialState: {
        queries: [],
        queryPannelOrientation: "0 0 0",
        requests: []
    },
    handlers: {
        receiveQuery(state, action) {
            console.log(action);
            //debugger;
            action.forEach(e => {
                state.queries.push(e);
            });
            state.queryPannelRotation = "0 " + (90 + (action.length - 1) * 45 / 2).toString() + " 0";
            //console.log(state.queryPannelRotation)
            /*
                //In case we want to work on the window history
                console.log(state.queries);
                state.requests = [];
                state.queries.forEach(query => {
                    state.requests.push(query[1].queryContext.originalQuery);
                });
                console.log(encodeURIComponent(JSON.stringify(state.requests)));
            */
        }

    }
})


