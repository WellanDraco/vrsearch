
AFRAME.registerState({
    initialState:{
        warning:{
            show: false,
            title:"No title",
            content:"No content",
            symbole:"warning"
        }
    },
    handlers:{
        showMessage(state,action){
            state.warning.show = true;
            
            //if there is a title, wich is required
            if (action.title) {
                state.warning.title = _.truncate( action.title, {
                    length: 43,
                });
            }
            else {
                state.warning.title = "missing title";
                console.warn("missing title");
            }

            //handle content
            state.warning.content = _.truncate(action.content || "", {
                length: 105,
            });

            //test if image is known
            const symboleValues = ["warning", "valid", "love"];
            if (_.indexOf(symboleValues, action.symbole || "")) {
                state.warning.symbole = action.symbole
            }
            else {
                state.warning.symbole = "#warning";
            }
            console.log(state);
        },
        hideMessage(state, action) {
            state.warning.show = false;
        }
    }
})