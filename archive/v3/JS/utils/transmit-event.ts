//This component cast events received to an other (or others) selected gameobject
//Events can be casted with a different type
AFRAME.registerComponent("transmit-event", {
    multiple: true,
    schema: {
        events: { default: [] },
        emitType: { default: "" }, //If we want to emit a event of different type to the target
        target: {
            default: "parent",
            oneOf: [
                "parent",
                "selector",
                "selectorAll",
                "localSelector",
                "localSelectorAll",
            ],
        },
        bubble: { default: false },
        useSetbubble: { default: false },
        selectorAll: { type: "selectorAll" },
        selector: { type: "selector" },
        localSelector: { default: "" },
        localSelectorAll: { default: "" },
    },
    init() {
        this.message = this.message.bind(this);
    },
    update(olddata) {
        let data = this.data;
        let oldE = olddata.events;
        let E = data.events;
        if (oldE) {
            oldE.forEach((event) => {
                this.el.removeEventListener(event, this.message);
            });
        }
        if (E) {
            E.forEach((event) => {
                this.el.addEventListener(event, this.message);
            });
        }
    },
    remove() {
        let E = this.data.events;
        if (E) {
            E.forEach((event) => {
                this.el.removeEventListener(event, this.message);
            });
        }
    },
    message(e) {
        //console.log(this.data);
        let d = this.data;
        //If the target is parent, we dont change the event type and the event bubbles, then it will already trigger on the parent anyway
        if (d.target == "parent" && e.bubbles && d.emitType == "") return;

        //We regroup the receivers based of it's type
        let emitReceivers; // = (d.target == 'parent')? [this.el.parentEl] : d[d.target];
        switch (d.target) {
            case "selector":
                emitReceivers = [d.selector];
                break;
            case "selectorAll":
                emitReceivers = d.selectorAll;
                break;
            case "localSelector":
                emitReceivers = [this.el.querySelector(d.localSelector)];
                break;
            case "localSelectorAll":
                //console.log(d.localSelectorAll);
                emitReceivers = this.el.querySelectorAll(d.localSelectorAll);
                break;
            default:
                emitReceivers = [this.el.parentEl];
                break;
        }
        //console.log(emitReceivers);
        //wether we change the type or not
        let type = d.emitType == "" ? e.type : d.emitType;

        //wether the event bubble or not
        let bubble = d.useSetbubble ? d.bubble : e.bubbles;

        emitReceivers.forEach((emitReceiver) => {
            emitReceiver.emit(type, e.detail, bubble);
        });
    },
});
