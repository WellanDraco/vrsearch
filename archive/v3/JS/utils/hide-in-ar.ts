AFRAME.registerComponent('hide-in-ar-mode', {
    // Set this object invisible while in AR mode.
    // TODO: could this be replaced with bind="visible: !ar-mode"
    // with https://www.npmjs.com/package/aframe-state-component ?
    init: function () {
      this.el.sceneEl.addEventListener('enter-vr', (ev) => {
        if (this.el.sceneEl.is('ar-mode')) {
          this.el.setAttribute('visible', false);
        }
      });
      this.el.sceneEl.addEventListener('exit-vr', (ev) => {
        this.el.setAttribute('visible', true);
      });
    }
  })