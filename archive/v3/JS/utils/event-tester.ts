AFRAME.registerComponent("event-tester", {
    schema: {
        events: {
            default: [],
        },
    },
    init() {
        this.message = this.message.bind(this);
    },
    update(olddata) {
        let oldE = olddata.events;
        let E = this.data.events;
        //console.log(E, oldE);
        if (oldE) {
            oldE.forEach((event) => {
                console.log("remove" + oldE.type);
                this.el.removeEventListener(event, this.message);
            });
        }

        if (E) {
            E.forEach((event) => {
                this.el.addEventListener(event, this.message);
            });
        }
    },
    message(e) {
        e.preventDefault();
        //console.log(e.type);
        console.log(e.type, e, this.el);
    },
});
