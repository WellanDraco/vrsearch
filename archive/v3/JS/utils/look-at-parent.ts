//Simple look at custom to follow the parent
AFRAME.registerComponent("look-at-parent", {
    init() {
        this.el.setAttribute("look-at", ""); //we create the component
        this.isLookSet = false;
        //console.log("heellooooo");
    },
    tick() {
        // we dont allways have the parent selector, soo let's manually trigger the tracking
        if (!this.isLookSet) {
            
            //console.log("heellooooo");
            let la = this.el.components["look-at"];
            if (la && this.el.parentEl) {
                //when component is ready
                la.beginTracking(this.el.parentEl);
                this.isLookSet = true;
                //console.dir(this.el);
            }
        }
    },
});
