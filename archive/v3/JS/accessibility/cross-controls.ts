AFRAME.registerComponent("cross-controls", {
    schema: {
        inline: { type: "selector", default: "#inlineCursor" },
        immersed: { type: "selector", default: "#immersedCursor" },
        keyboard: { type: "selector", default: "#keyboard" },
        lefthand: { type: "selector", default: "#left-hand" },
        righthand: { type: "selector", default: "#right-hand" },
        rigthhanded: { default: true },
    },
    init() {
        //console.log(this);
        this.inVR = false;

        this.enterVR = this.enterVR.bind(this);
        this.exitVR = this.exitVR.bind(this);
        this.el.sceneEl.addEventListener("enter-vr", this.enterVR);
        this.el.sceneEl.addEventListener("exit-vr", this.exitVR);

        this.enterVR = this.enterVR.bind(this);
        this.exitVR = this.exitVR.bind(this);
    },
    enterVR() {
        this.inVR = true;
        //console.log("enterVR");
        if (AFRAME.utils.device.isMobile()) {
            this.data.immersed.setAttribute("raycaster", "enabled", true);
            this.data.immersed.object3D.visible = true;
            this.data.keyboard.setAttribute(
                "super-keyboard",
                "hand",
                this.data.immersed
            );
        } else {
            this.data.keyboard.setAttribute(
                "super-keyboard",
                "hand",
                this.data.rigthhanded ? this.data.righthand : this.data.lefthand
            );
        }
    },
    update() {
        if (this.inVR) {
            this.data.keyboard.setAttribute(
                "super-keyboard",
                "hand",
                this.data.rigthhanded ? this.data.righthand : this.data.lefthand
            );
        }
    },
    exitVR() {
        this.inVR = false;
        //console.log("exitVR");
        this.data.immersed.object3D.visible = false;
        this.data.immersed.setAttribute("raycaster", "enabled", false);
        this.data.keyboard.setAttribute(
            "super-keyboard",
            "hand",
            this.data.inline
        );
    },
});
