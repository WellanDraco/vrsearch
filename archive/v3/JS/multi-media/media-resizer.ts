//Resize element to match size (be contained or fill the space)

AFRAME.registerComponent("media-resizer", {
    schema: {
        mediaWidth: { type: "float", default: 1 }, //original size
        mediaHeight: { type: "float", default: 1 }, //original size
        targetWidth: { type: "float", default: 1 }, //target volum
        targetHeight: { type: "float", default: 1 }, //target volum
        contains: { type: "boolean", default: true }, // true : match inside of target volum, false : fill target volime
    },
    update: function () {
        //Calcul bit to scale the media to optimal size
        let fw,
            fh,
            d = this.data,
            w = d.mediaWidth,
            h = d.mediaHeight,
            tw = d.targetWidth,
            th = d.targetHeight,
            contains = d.contains;

        let tRatio = tw / th;
        // console.log("tRatio",tRatio);
        let ratio = w / h;
        //console.log("ratio",ratio);

        //If contains, then we make sure the final dimensions stays inside the target area
        //otherwith we make sure the final dimensions fills the target area
        if (contains ? tRatio > ratio : tRatio < ratio) {
            //Heigth must fit
            //console.log("height");
            fh = th;
            fw = (w * th) / h;
        } else {
            //width must fit
            // console.log("width");

            fw = tw;
            fh = (h * tw) / w;
        }
        // console.log(fw,fh);

        this.el.setAttribute("geometry", {
            width: fw,
            height: fh,
        });

        //This only have an impact if the media is a video
        this.el.setAttribute("material", {
            width: fw,
            height: fh,
        });
    },
});
